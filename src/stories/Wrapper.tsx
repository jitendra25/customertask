import { Mutable, TemplateLoader } from '@saastack/core'
import { useQuery, withEnvironment } from '@saastack/relay'
import { Router } from '@saastack/router'
import React from 'react'
import { graphql } from 'relay-runtime'
import { WrapperQuery } from '../__generated__/WrapperQuery.graphql'

const query = graphql`
    query WrapperQuery {
        viewer {
            id
            firstName
            lastName
            email
            profileImage {
                thumbImage
            }
            preferences {
                dateFormat
                language
                timeFormat
                timezone
                uiInfo
            }
            userRoles {
                role {
                    levelDetails {
                        ... on Location {
                            id
                        }
                        ... on Company {
                            id
                        }
                        ... on Group {
                            id
                        }
                    }
                    role {
                        moduleRoles {
                            name
                        }
                    }
                }
            }
            groups {
                id
                name
                companies {
                    id
                    title
                    displayName
                    metadata
                    slugObject {
                        id
                        slugType
                        slugValue
                    }
                    gallery {
                        default {
                            thumbImage
                        }
                    }
                    address {
                        country
                    }
                    companySettings {
                        navMenus
                        aliases(locale: "en-US")
                    }
                    apps {
                        id
                        appTypeId
                        name
                        active
                        serviceModules
                    }
                    locations(first: 500) {
                        edges {
                            node {
                                id
                                name
                                preference {
                                    currency
                                }
                                slugObject {
                                    id
                                    slugType
                                    slugValue
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`

const Wrapper: React.FC = (props) => {
    const { data } = useQuery<WrapperQuery>(query, {}, { fetchPolicy: 'store-or-network' })
    if (!data) {
        return <>Loading...</>
    }
    return (
        <TemplateLoader
            getLocale={() => Promise.resolve({ messages: {} })}
            viewer={data?.viewer as Mutable<WrapperQuery['response']['viewer']>}
        >
            <Router>{props.children}</Router>
        </TemplateLoader>
    )
}

export default withEnvironment({ graphqlUrl: process.env.REACT_APP_GRAPHQL_URL })(Wrapper)
