import React from 'react'
import '../index'
import '@saastack/notes'
import CustomerPage from '../pages/CustomerPage'
import Wrapper from './Wrapper'

export default {
    title: 'Customers',
    decorators: [(storyFn: () => JSX.Element) => <Wrapper>{storyFn()}</Wrapper>],
}

export const Default = () => <CustomerPage />
