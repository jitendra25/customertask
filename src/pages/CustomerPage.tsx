import { ErrorComponent, Loading } from '@saastack/components'
import { useConfig } from '@saastack/core'
import { Roles, useCan } from '@saastack/core/roles'
import { useQuery } from '@saastack/relay'
import React from 'react'
import { graphql } from 'react-relay'
import { CustomerPageQuery } from '../__generated__/CustomerPageQuery.graphql'
import CustomerMaster from '../components/CustomerMaster'
import { PubSub } from '@saastack/pubsub'
import namespace from '../namespace'

const query = graphql`
    query CustomerPageQuery(
        $count: Int!
        $cursor: String
        $parent: String
        $filters: [ReportFilterInput]
        $locationIds: [String]
        $accessContact: Boolean
        $email: String
        $firstName: String
        $lastName: String
        $phoneNumber: String
        $tags: [String]
        $customerCompanyId: String
        $sortBy: CustomerListSortBy
        $direction: OrderBy
    ) {
        ...CustomerMaster_customers
            @arguments(
                count: $count
                cursor: $cursor
                parent: $parent
                filters: $filters
                locationIds: $locationIds
                accessContact: $accessContact
                email: $email
                firstName: $firstName
                lastName: $lastName
                phoneNumber: $phoneNumber
                tags: $tags
                customerCompanyId: $customerCompanyId
                sortBy: $sortBy
                direction: $direction
            )
    }
`

interface PageProps {
    parent?: string
}

const CustomerPage: React.FC<PageProps> = (props) => {
    const { locations, companyId, locationId } = useConfig()
    const can = useCan()
    const companyAdmin = can([], companyId!)
    const locationAdmin = can([], locationId!)
    const parent = (props.parent || companyId)!
    const locationIds = companyAdmin ? [] : locations?.map((l) => l.node!.id!) || []
    const accessContact = companyAdmin
        ? can([Roles.CustomersContactViewer], companyId!)
        : locationIds.every((lId) => can([Roles.CustomersContactViewer], lId!))
    const variables: CustomerPageQuery['variables'] = {
        count: 20,
        parent,
        locationIds,
        filters: [],
        accessContact,
    }
    const { data, loading, error, refetch } = useQuery<CustomerPageQuery>(query, variables)
    React.useEffect(() => {
        refetch()
    }, [parent])
    React.useEffect(() => {
        const id = PubSub.subscribe(namespace.create, () => refetch())
        return () => {
            PubSub.unsubscribe(id)
        }
    }, [])
    React.useEffect(() => {
        const id = PubSub.subscribe('notification/bulkimport', (data) => {
            const exist = data && data.eventType === 'saastack.background.BulkCustomer.Upload'
            if (Boolean(exist)) {
                refetch()
            }
        })
        return () => {
            PubSub.unsubscribe(id)
        }
    }, [])
    if (loading) {
        return <Loading />
    }
    if (error) {
        return <ErrorComponent error={error} />
    }
    return (
        <CustomerMaster
            {...props}
            companyAdmin={companyAdmin}
            locationAdmin={locationAdmin}
            customers={data!}
            parent={parent}
        />
    )
}

export default CustomerPage
