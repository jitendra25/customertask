import { graphql } from 'react-relay'

const CustomerReportQuery = graphql`
    query CustomerReportQuery(
        $dateRange: DateslotInput
        $exportReport: Boolean
        $customerTag: String
        $email: String
        $name: String
        $status: CustomerVerificationStatusFilterInput
        $limit: Int
        $locationIds: [String]
        $offset: Int
        $parent: String
        $timezone: String
        $export: Boolean
    ) {
        detailedCustomerSingupReport(
            dateRange: $dateRange
            exportReport: $exportReport
            customerTag: $customerTag
            name: $name
            email: $email
            status: $status
            limit: $limit
            locationIds: $locationIds
            offset: $offset
            parent: $parent
            timezone: $timezone
            export: $export
        ) {
            edges {
                node {
                    signupOn
                    firstName
                    lastName
                    email
                    telephones
                    birthDate
                    companyId
                    customerId
                    address {
                        country
                        locality
                        postalCode
                        region
                        streetAddress
                    }
                }
                cursor
            }
            pageInfo {
                nextOffset
                hasPreviousPage
                previousOffset
                hasNextPage
            }
            downloadLimit
            total
        }
    }
`

export default CustomerReportQuery
