import { graphql } from 'relay-runtime'

const GetCustomerQuery = graphql`
    query GetCustomerQuery($id: ID) {
        customer(id: $id) {
            id
        }
    }
`

export default GetCustomerQuery
