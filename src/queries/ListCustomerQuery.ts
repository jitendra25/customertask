import { graphql } from 'relay-runtime'

const ListCustomerQuery = graphql`
    query ListCustomerQuery($parent: String, $count: Int, $cursor: String) {
        customers(parent: $parent, first: $count, after: $cursor) {
            edges {
                node {
                    id
                    firstName
                    lastName
                    email
                    preferredLanguage
                    timezone
                    profileImage {
                        thumbImage
                        thumbImage
                    }
                }
            }
        }
    }
`

export default ListCustomerQuery
