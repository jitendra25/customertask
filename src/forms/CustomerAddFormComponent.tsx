import { t, Trans } from '@lingui/macro'
import { Box, Collapse, Grid, Theme } from '@material-ui/core'
import { fade } from '@material-ui/core/styles/colorManipulator'
import { BusinessOutlined, EventOutlined, MapOutlined, PlaceOutlined } from '@material-ui/icons'
import { makeStyles } from '@material-ui/styles'
import { useAlias, useConfig } from '@saastack/core'
import { Roles, useCan } from '@saastack/core/roles'
import {
    Address,
    Checkbox,
    Datepicker,
    Form,
    FormObserver,
    Input,
    PhoneNumber,
} from '@saastack/forms'
import { InputProps } from '@saastack/forms/Input'
import { FormProps } from '@saastack/forms/types'
import { findFocusableElement } from '@saastack/utils'
import { some } from 'lodash-es'
import React from 'react'
import { FormikContextType } from 'formik/dist/types'
import { CustomerInput } from '../__generated__/CreateCustomerMutation.graphql'
import LocationWidget from '@saastack/locations/components/LocationWidget'

export interface Props<T> extends FormProps<T> {
    add?: boolean
}

const useStyles = makeStyles(({ spacing, palette: { text } }: Theme) => ({
    root: {
        '& .MuiExpansionPanel-root.Mui-expanded': {
            margin: 0,
        },
        '& .MuiExpansionPanelDetails-root': {
            padding: spacing(1, 0),
        },
        '& .MuiExpansionPanel-root.Mui-expanded + .MuiExpansionPanel-root:before': {
            display: 'none',
        },
        '& > .MuiGrid-container > .MuiBox-root': {
            flex: '0 0 100%',
            marginTop: spacing(1),
        },
    },
    grid: {
        margin: 0,
        width: '100%',
        padding: `0px!important`,
    },
    grid2: {
        margin: 0,
        '&.MuiGrid-spacing-xs-1 > .MuiGrid-item': {
            padding: spacing(0, 0.5),
        },
    },
    ic: {
        maxHeight: 36,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& .MuiSvgIcon-root': {
            color: fade(text.primary, 0.64),
        },
    },
    input: {
        '& > .MuiInputBase-root.MuiFilledInput-root.MuiAutocomplete-inputRoot': {
            padding: `10.5px 65px 10.5px 12px`,
            '& > .MuiInputBase-input.MuiFilledInput-input.MuiAutocomplete-input': {
                padding: 0,
            },
        },
    },
    collapse: {
        width: '100%',
    },
    location: {
        '& .MuiFormControl-root': {
            marginTop: '0px!important',
            marginBottom: '0px!important',
        },
        '& .MuiInputBase-root': {
            paddingLeft: '4px!important',
            paddingRight: '4px!important',
        },
    },
}))

const hasAnyValue = (values: any) => some(values, (v) => Boolean(v))

const CustomerAddFormComponent = <T extends {}>(props: Props<T>): React.ReactElement<Props<T>> => {
    const classes = useStyles()
    const { company, locations, companyId } = useConfig()
    const canManage = useCan()([Roles.CustomersAdmin, Roles.CustomersEditor], companyId!)
    const [showAddress, setShowAddress] = React.useState(
        hasAnyValue((props.initialValues as any).address)
    )
    const locationAlias = useAlias('Locations', {
        singular: 'Location',
        plural: 'Locations',
    })
    const al = locationAlias?.singular

    const [email, setEmail] = React.useState(false)
    const [sms, setSms] = React.useState(false)

    const inputProps = (key: string): InputProps => {
        let placeholder: any = ''
        switch (key) {
            case 'streetAddress':
                placeholder = t`Street Address`
                break
            case 'locality':
                placeholder = t`City`
                break
            case 'region':
                placeholder = t`State`
                break
            case 'country':
                placeholder = t`Country`
                break
            case 'postalCode':
                placeholder = t`Postal Code`
                break
        }
        return {
            variant: 'inline',
            margin: 'none',
            className: classes.input,
            placeholder,
            label: null,
        }
    }
    const handleAddressOpen = (node: HTMLElement) => {
        const el = findFocusableElement(node)
        if (el) {
            el.focus()
        }
    }

    const handleChange = ({ values, errors, setErrors }: FormikContextType<CustomerInput>) => {
        if (values.email!.length > 1) {
            if (!email) setEmail(true)
        } else {
            if (email) setEmail(false)
        }
        if (values.telephones![0]!?.length > 3) {
            if (!sms) setSms(true)
        } else {
            if (sms) setSms(false)
        }
    }

    return (
        <Form className={classes.root} {...props}>
            <Input variant="standard" name="name" placeholder={t`Name`} grid={{ xs: 12 }} />
            <Input variant="standard" name="email" placeholder={t`Email`} grid={{ xs: 12 }} />
            <PhoneNumber
                variant="standard"
                name="telephones.0"
                placeholder={t`Phone Number`}
                defaultCountry={company!.address!.country}
                grid={{ xs: 12 }}
            />
            <Grid item xs={12}>
                <Box my={3} />
            </Grid>
            <Grid item xs={1} className={classes.ic}>
                <BusinessOutlined />
            </Grid>
            <Input
                margin="none"
                variant="inline"
                name="companyId"
                placeholder={t`Add company (optional)`}
                grid={{ xs: 11 }}
            />
            {!(props.initialValues as any).id && locations?.length! > 1 && (
                <>
                    <Grid item xs={1} className={classes.ic}>
                        <PlaceOutlined />
                    </Grid>
                    <LocationWidget
                        multiple={true}
                        name="locationIds"
                        searchProps={{
                            placeholder: canManage
                                ? !!al
                                    ? t`${al} (optional)`
                                    : t`Location (optional)`
                                : al
                                ? t`${al}`
                                : t`Location`,
                            variant: 'inline' as any,
                        }}
                        grid={{ xs: 11 }}
                        className={classes.location}
                    />
                </>
            )}
            <Grid item xs={1} className={classes.ic}>
                <EventOutlined />
            </Grid>
            <Datepicker
                InputProps={{ margin: 'none', variant: 'inline' }}
                name="birthDate"
                placeholder={t`Add date of birth (optional)`}
                grid={{ xs: 11 }}
                disableFuture
                startDate={'1990-01-01T00:00:00.000Z'}
            />
            <Grid item xs={1} className={classes.ic}>
                <MapOutlined />
            </Grid>
            <Grid className={classes.grid} item container spacing={1} xs={11}>
                <Collapse
                    onEntered={handleAddressOpen}
                    className={classes.collapse}
                    in={showAddress}
                >
                    <Grid className={classes.grid2} item container spacing={1} xs={12}>
                        <Address name="address" InputProps={inputProps} />
                    </Grid>
                </Collapse>
                <Collapse className={classes.collapse} in={!showAddress}>
                    <Grid className={classes.grid2} item container spacing={1} xs={12}>
                        <Input
                            onFocus={() => setShowAddress(true)}
                            margin="none"
                            variant="inline"
                            placeholder={t`Add address, city, state, country`}
                            grid={{ xs: 12 }}
                        />
                    </Grid>
                </Collapse>
            </Grid>
            <Collapse in={Boolean(props.add)}>
                <Box mt={2}>
                    <Collapse in={email}>
                        <Checkbox
                            name="sendEmail"
                            label={<Trans>Notify via email</Trans>}
                            grid={{ xs: 12 }}
                            size="small"
                        />
                    </Collapse>
                    <Collapse in={sms}>
                        <Checkbox
                            name="sendSms"
                            label={<Trans>Notify via sms</Trans>}
                            grid={{ xs: 12 }}
                            size="small"
                        />
                    </Collapse>
                </Box>
            </Collapse>
            <FormObserver debounce={1} onChange={handleChange} />
        </Form>
    )
}

export default CustomerAddFormComponent
