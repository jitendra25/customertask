/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type CustomerList_customers = ReadonlyArray<{
    readonly id: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly email: string;
    readonly telephones: ReadonlyArray<string>;
    readonly createdOn: string | null;
    readonly " $fragmentRefs": FragmentRefs<"CustomerAvatar_customer">;
    readonly " $refType": "CustomerList_customers";
}>;
export type CustomerList_customers$data = CustomerList_customers;
export type CustomerList_customers$key = ReadonlyArray<{
    readonly " $data"?: CustomerList_customers$data;
    readonly " $fragmentRefs": FragmentRefs<"CustomerList_customers">;
}>;



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": {
    "plural": true
  },
  "name": "CustomerList_customers",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "firstName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "lastName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "email",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "telephones",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdOn",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "CustomerAvatar_customer"
    }
  ],
  "type": "Customer",
  "abstractKey": null
};
(node as any).hash = '819438b94e4b9383cb3e5dc9f10fb9c7';
export default node;
