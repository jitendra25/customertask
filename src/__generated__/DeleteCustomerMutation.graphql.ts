/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type DeleteCustomerInput = {
    clientMutationId?: string | null;
    id?: string | null;
};
export type DeleteCustomerMutationVariables = {
    input?: DeleteCustomerInput | null;
};
export type DeleteCustomerMutationResponse = {
    readonly deleteCustomer: {
        readonly clientMutationId: string;
    };
};
export type DeleteCustomerMutation = {
    readonly response: DeleteCustomerMutationResponse;
    readonly variables: DeleteCustomerMutationVariables;
};



/*
mutation DeleteCustomerMutation(
  $input: DeleteCustomerInput
) {
  deleteCustomer(input: $input) {
    clientMutationId
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "DeleteCustomerPayload",
    "kind": "LinkedField",
    "name": "deleteCustomer",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "clientMutationId",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "DeleteCustomerMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "DeleteCustomerMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "e92c02669aa277d8e3d9ee288b66e8be",
    "id": null,
    "metadata": {},
    "name": "DeleteCustomerMutation",
    "operationKind": "mutation",
    "text": "mutation DeleteCustomerMutation(\n  $input: DeleteCustomerInput\n) {\n  deleteCustomer(input: $input) {\n    clientMutationId\n  }\n}\n"
  }
};
})();
(node as any).hash = '5f877fcb5cc64e7d48b26f4d5d8eace4';
export default node;
