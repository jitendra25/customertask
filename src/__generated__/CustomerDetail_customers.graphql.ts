/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type CustomerDetail_customers = ReadonlyArray<{
    readonly id: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly email: string;
    readonly profileImage: {
        readonly largeImage: string;
        readonly thumbImage: string;
    } | null;
    readonly " $fragmentRefs": FragmentRefs<"CustomerInfo_customer" | "CustomerStats_customer" | "CustomerAvatar_customer">;
    readonly " $refType": "CustomerDetail_customers";
}>;
export type CustomerDetail_customers$data = CustomerDetail_customers;
export type CustomerDetail_customers$key = ReadonlyArray<{
    readonly " $data"?: CustomerDetail_customers$data;
    readonly " $fragmentRefs": FragmentRefs<"CustomerDetail_customers">;
}>;



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": {
    "plural": true
  },
  "name": "CustomerDetail_customers",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "firstName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "lastName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "email",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "GalleryItem",
      "kind": "LinkedField",
      "name": "profileImage",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "largeImage",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "thumbImage",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "CustomerInfo_customer"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "CustomerStats_customer"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "CustomerAvatar_customer"
    }
  ],
  "type": "Customer",
  "abstractKey": null
};
(node as any).hash = '786c7527db7da1886b23819d51b859c4';
export default node;
