/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type BooleanFilter = "ALL" | "FALSE" | "TRUE" | "%future added value";
export type DateslotInput = {
    endTime?: string | null;
    startTime?: string | null;
};
export type CustomerVerificationStatusFilterInput = {
    emailVerified?: BooleanFilter | null;
    phoneVerified?: BooleanFilter | null;
};
export type CustomerReportQueryVariables = {
    dateRange?: DateslotInput | null;
    exportReport?: boolean | null;
    customerTag?: string | null;
    email?: string | null;
    name?: string | null;
    status?: CustomerVerificationStatusFilterInput | null;
    limit?: number | null;
    locationIds?: Array<string | null> | null;
    offset?: number | null;
    parent?: string | null;
    timezone?: string | null;
    export?: boolean | null;
};
export type CustomerReportQueryResponse = {
    readonly detailedCustomerSingupReport: {
        readonly edges: ReadonlyArray<{
            readonly node: {
                readonly signupOn: string | null;
                readonly firstName: string;
                readonly lastName: string;
                readonly email: string;
                readonly telephones: ReadonlyArray<string>;
                readonly birthDate: string | null;
                readonly companyId: string;
                readonly customerId: string;
                readonly address: {
                    readonly country: string;
                    readonly locality: string;
                    readonly postalCode: string;
                    readonly region: string;
                    readonly streetAddress: string;
                } | null;
            } | null;
            readonly cursor: string;
        }>;
        readonly pageInfo: {
            readonly nextOffset: number;
            readonly hasPreviousPage: boolean;
            readonly previousOffset: number;
            readonly hasNextPage: boolean;
        } | null;
        readonly downloadLimit: number;
        readonly total: number;
    };
};
export type CustomerReportQuery = {
    readonly response: CustomerReportQueryResponse;
    readonly variables: CustomerReportQueryVariables;
};



/*
query CustomerReportQuery(
  $dateRange: DateslotInput
  $exportReport: Boolean
  $customerTag: String
  $email: String
  $name: String
  $status: CustomerVerificationStatusFilterInput
  $limit: Int
  $locationIds: [String]
  $offset: Int
  $parent: String
  $timezone: String
  $export: Boolean
) {
  detailedCustomerSingupReport(dateRange: $dateRange, exportReport: $exportReport, customerTag: $customerTag, name: $name, email: $email, status: $status, limit: $limit, locationIds: $locationIds, offset: $offset, parent: $parent, timezone: $timezone, export: $export) {
    edges {
      node {
        signupOn
        firstName
        lastName
        email
        telephones
        birthDate
        companyId
        customerId
        address {
          country
          locality
          postalCode
          region
          streetAddress
        }
      }
      cursor
    }
    pageInfo {
      nextOffset
      hasPreviousPage
      previousOffset
      hasNextPage
    }
    downloadLimit
    total
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "customerTag"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "dateRange"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "email"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "export"
},
v4 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "exportReport"
},
v5 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "limit"
},
v6 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "locationIds"
},
v7 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "name"
},
v8 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "offset"
},
v9 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "parent"
},
v10 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "status"
},
v11 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "timezone"
},
v12 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "customerTag",
        "variableName": "customerTag"
      },
      {
        "kind": "Variable",
        "name": "dateRange",
        "variableName": "dateRange"
      },
      {
        "kind": "Variable",
        "name": "email",
        "variableName": "email"
      },
      {
        "kind": "Variable",
        "name": "export",
        "variableName": "export"
      },
      {
        "kind": "Variable",
        "name": "exportReport",
        "variableName": "exportReport"
      },
      {
        "kind": "Variable",
        "name": "limit",
        "variableName": "limit"
      },
      {
        "kind": "Variable",
        "name": "locationIds",
        "variableName": "locationIds"
      },
      {
        "kind": "Variable",
        "name": "name",
        "variableName": "name"
      },
      {
        "kind": "Variable",
        "name": "offset",
        "variableName": "offset"
      },
      {
        "kind": "Variable",
        "name": "parent",
        "variableName": "parent"
      },
      {
        "kind": "Variable",
        "name": "status",
        "variableName": "status"
      },
      {
        "kind": "Variable",
        "name": "timezone",
        "variableName": "timezone"
      }
    ],
    "concreteType": "GetDetailedCustomerSignupReportResponse",
    "kind": "LinkedField",
    "name": "detailedCustomerSingupReport",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "DetailedCustomerSignupReportNode",
        "kind": "LinkedField",
        "name": "edges",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "DetailedCustomerSignupReportObject",
            "kind": "LinkedField",
            "name": "node",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "signupOn",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "firstName",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "lastName",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "email",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "telephones",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "birthDate",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "companyId",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "customerId",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Address",
                "kind": "LinkedField",
                "name": "address",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "country",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "locality",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "postalCode",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "region",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "streetAddress",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "cursor",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "PaginationInfo",
        "kind": "LinkedField",
        "name": "pageInfo",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "nextOffset",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "hasPreviousPage",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "previousOffset",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "hasNextPage",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "downloadLimit",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "total",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/),
      (v4/*: any*/),
      (v5/*: any*/),
      (v6/*: any*/),
      (v7/*: any*/),
      (v8/*: any*/),
      (v9/*: any*/),
      (v10/*: any*/),
      (v11/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "CustomerReportQuery",
    "selections": (v12/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v4/*: any*/),
      (v0/*: any*/),
      (v2/*: any*/),
      (v7/*: any*/),
      (v10/*: any*/),
      (v5/*: any*/),
      (v6/*: any*/),
      (v8/*: any*/),
      (v9/*: any*/),
      (v11/*: any*/),
      (v3/*: any*/)
    ],
    "kind": "Operation",
    "name": "CustomerReportQuery",
    "selections": (v12/*: any*/)
  },
  "params": {
    "cacheID": "e74f8589282b6be19e898b9d77640d98",
    "id": null,
    "metadata": {},
    "name": "CustomerReportQuery",
    "operationKind": "query",
    "text": "query CustomerReportQuery(\n  $dateRange: DateslotInput\n  $exportReport: Boolean\n  $customerTag: String\n  $email: String\n  $name: String\n  $status: CustomerVerificationStatusFilterInput\n  $limit: Int\n  $locationIds: [String]\n  $offset: Int\n  $parent: String\n  $timezone: String\n  $export: Boolean\n) {\n  detailedCustomerSingupReport(dateRange: $dateRange, exportReport: $exportReport, customerTag: $customerTag, name: $name, email: $email, status: $status, limit: $limit, locationIds: $locationIds, offset: $offset, parent: $parent, timezone: $timezone, export: $export) {\n    edges {\n      node {\n        signupOn\n        firstName\n        lastName\n        email\n        telephones\n        birthDate\n        companyId\n        customerId\n        address {\n          country\n          locality\n          postalCode\n          region\n          streetAddress\n        }\n      }\n      cursor\n    }\n    pageInfo {\n      nextOffset\n      hasPreviousPage\n      previousOffset\n      hasNextPage\n    }\n    downloadLimit\n    total\n  }\n}\n"
  }
};
})();
(node as any).hash = '6aa74618ecb837eac8a629579304acc3';
export default node;
