/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type AmountType = "ARPV" | "LTV" | "TOTAL_APPOINTMENT_AMOUNT" | "TOTAL_CLASS_AMOUNT" | "TOTAL_GIFT_CERTIFICATE_AMOUNT" | "TOTAL_MEMBERSHIP_AMOUNT" | "TOTAL_PACKAGE_AMOUNT" | "UNSPECIFIED_AMOUNT_TYPE" | "%future added value";
export type NumberType = "TOTAL_APPOINTMENTS" | "TOTAL_CLASSES" | "TOTAL_GIFT_CERTIFICATES" | "TOTAL_MEMBERSHIPS" | "TOTAL_PACKAGES" | "TOTAL_VISITS" | "UNSPECIFIED_NUMBER_TYPE" | "%future added value";
export type CustomerStatsQueryVariables = {
    customerId?: string | null;
    startDate?: string | null;
    endDate?: string | null;
    timezone?: string | null;
    parent?: string | null;
};
export type CustomerStatsQueryResponse = {
    readonly customerAmounts: {
        readonly customerAmounts: ReadonlyArray<{
            readonly amount: {
                readonly amount: number;
                readonly currency: string;
            } | null;
            readonly amountType: AmountType;
            readonly appName: string;
        }>;
    };
    readonly customerNumbers: {
        readonly customerNumbers: ReadonlyArray<{
            readonly number: number;
            readonly numberType: NumberType;
            readonly appName: string;
        }>;
    };
    readonly salesReportByCustomer: {
        readonly data: ReadonlyArray<{
            readonly date: string | null;
            readonly sales: {
                readonly amount: number;
                readonly currency: string;
            } | null;
        }>;
    };
};
export type CustomerStatsQuery = {
    readonly response: CustomerStatsQueryResponse;
    readonly variables: CustomerStatsQueryVariables;
};



/*
query CustomerStatsQuery(
  $customerId: String
  $startDate: Timestamp
  $endDate: Timestamp
  $timezone: String
  $parent: String
) {
  customerAmounts(customerId: $customerId, parent: $parent) {
    customerAmounts {
      amount {
        amount
        currency
      }
      amountType
      appName
    }
  }
  customerNumbers(customerId: $customerId, parent: $parent) {
    customerNumbers {
      number
      numberType
      appName
    }
  }
  salesReportByCustomer(customerId: $customerId, startDate: $startDate, endDate: $endDate, timezone: $timezone, parent: $parent) {
    data {
      date
      sales {
        amount
        currency
      }
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "customerId"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "endDate"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "parent"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "startDate"
},
v4 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "timezone"
},
v5 = {
  "kind": "Variable",
  "name": "customerId",
  "variableName": "customerId"
},
v6 = {
  "kind": "Variable",
  "name": "parent",
  "variableName": "parent"
},
v7 = [
  (v5/*: any*/),
  (v6/*: any*/)
],
v8 = [
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "amount",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "currency",
    "storageKey": null
  }
],
v9 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "appName",
  "storageKey": null
},
v10 = [
  {
    "alias": null,
    "args": (v7/*: any*/),
    "concreteType": "GetCustomerAmountsResponse",
    "kind": "LinkedField",
    "name": "customerAmounts",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "CustomerAmount",
        "kind": "LinkedField",
        "name": "customerAmounts",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Price",
            "kind": "LinkedField",
            "name": "amount",
            "plural": false,
            "selections": (v8/*: any*/),
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "amountType",
            "storageKey": null
          },
          (v9/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  },
  {
    "alias": null,
    "args": (v7/*: any*/),
    "concreteType": "GetCustomerNumbersResponse",
    "kind": "LinkedField",
    "name": "customerNumbers",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "CustomerNumber",
        "kind": "LinkedField",
        "name": "customerNumbers",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "number",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "numberType",
            "storageKey": null
          },
          (v9/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  },
  {
    "alias": null,
    "args": [
      (v5/*: any*/),
      {
        "kind": "Variable",
        "name": "endDate",
        "variableName": "endDate"
      },
      (v6/*: any*/),
      {
        "kind": "Variable",
        "name": "startDate",
        "variableName": "startDate"
      },
      {
        "kind": "Variable",
        "name": "timezone",
        "variableName": "timezone"
      }
    ],
    "concreteType": "GetMonthlySalesReportByCustomerResponse",
    "kind": "LinkedField",
    "name": "salesReportByCustomer",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "MonthlySalesReportByCustomer",
        "kind": "LinkedField",
        "name": "data",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "date",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Price",
            "kind": "LinkedField",
            "name": "sales",
            "plural": false,
            "selections": (v8/*: any*/),
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/),
      (v4/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "CustomerStatsQuery",
    "selections": (v10/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v3/*: any*/),
      (v1/*: any*/),
      (v4/*: any*/),
      (v2/*: any*/)
    ],
    "kind": "Operation",
    "name": "CustomerStatsQuery",
    "selections": (v10/*: any*/)
  },
  "params": {
    "cacheID": "4a44a9e7ea4df78190b8375f37506008",
    "id": null,
    "metadata": {},
    "name": "CustomerStatsQuery",
    "operationKind": "query",
    "text": "query CustomerStatsQuery(\n  $customerId: String\n  $startDate: Timestamp\n  $endDate: Timestamp\n  $timezone: String\n  $parent: String\n) {\n  customerAmounts(customerId: $customerId, parent: $parent) {\n    customerAmounts {\n      amount {\n        amount\n        currency\n      }\n      amountType\n      appName\n    }\n  }\n  customerNumbers(customerId: $customerId, parent: $parent) {\n    customerNumbers {\n      number\n      numberType\n      appName\n    }\n  }\n  salesReportByCustomer(customerId: $customerId, startDate: $startDate, endDate: $endDate, timezone: $timezone, parent: $parent) {\n    data {\n      date\n      sales {\n        amount\n        currency\n      }\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = '8ecf72f8b1bb8d6c8ae950d8512509a0';
export default node;
