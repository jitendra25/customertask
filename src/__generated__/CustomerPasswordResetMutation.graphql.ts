/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type CustomerPasswordResetInput = {
    clientMutationId?: string | null;
    customerId?: string | null;
};
export type CustomerPasswordResetMutationVariables = {
    input?: CustomerPasswordResetInput | null;
};
export type CustomerPasswordResetMutationResponse = {
    readonly customerPasswordReset: {
        readonly clientMutationId: string;
        readonly payload: {};
    };
};
export type CustomerPasswordResetMutation = {
    readonly response: CustomerPasswordResetMutationResponse;
    readonly variables: CustomerPasswordResetMutationVariables;
};



/*
mutation CustomerPasswordResetMutation(
  $input: CustomerPasswordResetInput
) {
  customerPasswordReset(input: $input) {
    clientMutationId
    payload
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "CustomerPasswordResetPayload",
    "kind": "LinkedField",
    "name": "customerPasswordReset",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "clientMutationId",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "payload",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "CustomerPasswordResetMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "CustomerPasswordResetMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "3252c756f9f9560be583e3dc62c817db",
    "id": null,
    "metadata": {},
    "name": "CustomerPasswordResetMutation",
    "operationKind": "mutation",
    "text": "mutation CustomerPasswordResetMutation(\n  $input: CustomerPasswordResetInput\n) {\n  customerPasswordReset(input: $input) {\n    clientMutationId\n    payload\n  }\n}\n"
  }
};
})();
(node as any).hash = '5c417f315d832c148cfe87bb34bfbe33';
export default node;
