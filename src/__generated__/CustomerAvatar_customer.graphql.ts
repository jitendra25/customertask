/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type CustomerAvatar_customer = {
    readonly id: string;
    readonly firstName: string;
    readonly profileImage: {
        readonly largeImage: string;
        readonly thumbImage: string;
    } | null;
    readonly " $refType": "CustomerAvatar_customer";
};
export type CustomerAvatar_customer$data = CustomerAvatar_customer;
export type CustomerAvatar_customer$key = {
    readonly " $data"?: CustomerAvatar_customer$data;
    readonly " $fragmentRefs": FragmentRefs<"CustomerAvatar_customer">;
};



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "CustomerAvatar_customer",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "firstName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "GalleryItem",
      "kind": "LinkedField",
      "name": "profileImage",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "largeImage",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "thumbImage",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Customer",
  "abstractKey": null
};
(node as any).hash = 'bbdd1df5ef1f60fe66f2dedb6fe6c87e';
export default node;
