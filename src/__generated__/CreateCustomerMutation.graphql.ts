/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type Gender = "FEMALE" | "MALE" | "UNSPECIFIED" | "%future added value";
export type CreateCustomerInput = {
    clientMutationId?: string | null;
    customer?: CustomerInput | null;
    locationIds?: Array<string | null> | null;
    parent?: string | null;
    sendNotification?: CustomerSendNotificationInput | null;
};
export type CustomerInput = {
    address?: AddressInput | null;
    birthDate?: string | null;
    campaign?: string | null;
    companyId?: string | null;
    createdBy?: string | null;
    createdOn?: string | null;
    customerLevel?: string | null;
    email?: string | null;
    firstName?: string | null;
    gender?: Gender | null;
    id?: string | null;
    invitedBy?: string | null;
    isInvitedByAdmin?: boolean | null;
    lastName?: string | null;
    medium?: string | null;
    metadata?: string | null;
    preferredLanguage?: string | null;
    profileImage?: GalleryItemInput | null;
    source?: string | null;
    ssoId?: string | null;
    tag?: Array<string | null> | null;
    telephones?: Array<string | null> | null;
    timezone?: string | null;
    updatedBy?: string | null;
    updatedOn?: string | null;
    userId?: string | null;
};
export type AddressInput = {
    country?: string | null;
    latitude?: number | null;
    locality?: string | null;
    longitude?: number | null;
    postalCode?: string | null;
    region?: string | null;
    streetAddress?: string | null;
};
export type GalleryItemInput = {
    largeImage?: string | null;
    thumbImage?: string | null;
};
export type CustomerSendNotificationInput = {
    email?: boolean | null;
    sms?: boolean | null;
};
export type CreateCustomerMutationVariables = {
    input?: CreateCustomerInput | null;
};
export type CreateCustomerMutationResponse = {
    readonly createCustomer: {
        readonly clientMutationId: string;
        readonly payload: {
            readonly id: string;
            readonly firstName: string;
            readonly lastName: string;
            readonly email: string;
        };
    };
};
export type CreateCustomerMutation = {
    readonly response: CreateCustomerMutationResponse;
    readonly variables: CreateCustomerMutationVariables;
};



/*
mutation CreateCustomerMutation(
  $input: CreateCustomerInput
) {
  createCustomer(input: $input) {
    clientMutationId
    payload {
      id
      firstName
      lastName
      email
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "CreateCustomerPayload",
    "kind": "LinkedField",
    "name": "createCustomer",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "clientMutationId",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Customer",
        "kind": "LinkedField",
        "name": "payload",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "firstName",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "lastName",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "email",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "CreateCustomerMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "CreateCustomerMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "2cbc146f85138bdb791c00ebe4316438",
    "id": null,
    "metadata": {},
    "name": "CreateCustomerMutation",
    "operationKind": "mutation",
    "text": "mutation CreateCustomerMutation(\n  $input: CreateCustomerInput\n) {\n  createCustomer(input: $input) {\n    clientMutationId\n    payload {\n      id\n      firstName\n      lastName\n      email\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = '9966f34b3d9d00b0a9b69cd512385f13';
export default node;
