/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type BatchDeleteCustomerInput = {
    clientMutationId?: string | null;
    ids?: Array<string | null> | null;
};
export type BatchDeleteCustomerMutationVariables = {
    input?: BatchDeleteCustomerInput | null;
};
export type BatchDeleteCustomerMutationResponse = {
    readonly batchDeleteCustomer: {
        readonly clientMutationId: string;
        readonly payload: {};
    };
};
export type BatchDeleteCustomerMutation = {
    readonly response: BatchDeleteCustomerMutationResponse;
    readonly variables: BatchDeleteCustomerMutationVariables;
};



/*
mutation BatchDeleteCustomerMutation(
  $input: BatchDeleteCustomerInput
) {
  batchDeleteCustomer(input: $input) {
    clientMutationId
    payload
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "BatchDeleteCustomerPayload",
    "kind": "LinkedField",
    "name": "batchDeleteCustomer",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "clientMutationId",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "payload",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "BatchDeleteCustomerMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "BatchDeleteCustomerMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "65193589a25932156e7bad770598573f",
    "id": null,
    "metadata": {},
    "name": "BatchDeleteCustomerMutation",
    "operationKind": "mutation",
    "text": "mutation BatchDeleteCustomerMutation(\n  $input: BatchDeleteCustomerInput\n) {\n  batchDeleteCustomer(input: $input) {\n    clientMutationId\n    payload\n  }\n}\n"
  }
};
})();
(node as any).hash = '97f4082ee72458f0b0ad9e4c31469ef8';
export default node;
