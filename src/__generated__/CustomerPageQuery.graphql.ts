/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type CustomerListSortBy = "CreatedOn" | "Email" | "FirstName" | "LastName" | "%future added value";
export type OrderBy = "Ascending" | "Descending" | "%future added value";
export type ReportFields = "ADDED_ON_FILTER" | "ARPV_FILTER" | "LAST_ACTIVITY_ON_FILTER" | "LAST_APPOINTMENT_BOOKED_ON_FILTER" | "LAST_BOOKED_ON_FILTER" | "LAST_CLASS_BOOKED_ON_FILTER" | "LAST_PURCHASED_ON_FILTER" | "LAST_REVIEWED_ON_FILTER" | "LTV_FILTER" | "MOOD_FILTER" | "TOTAL_APPOINTMENTS_FILTER" | "TOTAL_APPOINTMENT_AMOUNT_FILTER" | "TOTAL_CLASSES_FILTER" | "TOTAL_CLASS_AMOUNT_FILTER" | "TOTAL_GIFT_CERTIFICATES_FILTER" | "TOTAL_GIFT_CERTIFICATE_AMOUNT_FILTER" | "TOTAL_MEMBERSHIPS_FILTER" | "TOTAL_MEMBERSHIP_AMOUNT_FILTER" | "TOTAL_PACKAGES_FILTER" | "TOTAL_PACKAGE_AMOUNT_FILTER" | "UNSPECIFIED" | "%future added value";
export type ReportOperator = "EQUAL" | "GREATER_THAN" | "IN_DATE" | "LESS_THAN" | "NOT_IN_DATE" | "UNSPECIFIED_REPORT_OPERATOR" | "%future added value";
export type ReportFilterInput = {
    endDate?: string | null;
    fieldName?: ReportFields | null;
    operator?: ReportOperator | null;
    startDate?: string | null;
    value?: number | null;
};
export type CustomerPageQueryVariables = {
    count: number;
    cursor?: string | null;
    parent?: string | null;
    filters?: Array<ReportFilterInput | null> | null;
    locationIds?: Array<string | null> | null;
    accessContact?: boolean | null;
    email?: string | null;
    firstName?: string | null;
    lastName?: string | null;
    phoneNumber?: string | null;
    tags?: Array<string | null> | null;
    customerCompanyId?: string | null;
    sortBy?: CustomerListSortBy | null;
    direction?: OrderBy | null;
};
export type CustomerPageQueryResponse = {
    readonly " $fragmentRefs": FragmentRefs<"CustomerMaster_customers">;
};
export type CustomerPageQuery = {
    readonly response: CustomerPageQueryResponse;
    readonly variables: CustomerPageQueryVariables;
};



/*
query CustomerPageQuery(
  $count: Int!
  $cursor: String
  $parent: String
  $filters: [ReportFilterInput]
  $locationIds: [String]
  $accessContact: Boolean
  $email: String
  $firstName: String
  $lastName: String
  $phoneNumber: String
  $tags: [String]
  $customerCompanyId: String
  $sortBy: CustomerListSortBy
  $direction: OrderBy
) {
  ...CustomerMaster_customers_3Tz1FU
}

fragment CustomerAvatar_customer on Customer {
  id
  firstName
  profileImage {
    largeImage
    thumbImage
  }
}

fragment CustomerDetail_customers on Customer {
  id
  firstName
  lastName
  email
  profileImage {
    largeImage
    thumbImage
  }
  ...CustomerInfo_customer
  ...CustomerStats_customer
  ...CustomerAvatar_customer
}

fragment CustomerInfo_customer on Customer {
  id
  companyId
  address {
    country
    locality
    postalCode
    region
    streetAddress
  }
  birthDate
  telephones
  timezone
  tag
  preferredLanguage
}

fragment CustomerList_customers on Customer {
  id
  firstName
  lastName
  email
  telephones
  createdOn
  ...CustomerAvatar_customer
}

fragment CustomerMaster_customers_3Tz1FU on Query {
  customers(parent: $parent, first: $count, after: $cursor, locationIds: $locationIds, filters: $filters, accessContact: $accessContact, email: $email, firstName: $firstName, lastName: $lastName, customerCompanyId: $customerCompanyId, phoneNumber: $phoneNumber, tags: $tags, sortBy: $sortBy, direction: $direction) {
    edges {
      node {
        ...CustomerList_customers
        ...CustomerDetail_customers
        ...CustomerUpdate_customers
        id
        __typename
      }
      cursor
    }
    pageInfo {
      endCursor
      startCursor
      hasNextPage
      hasPreviousPage
    }
  }
}

fragment CustomerStats_customer on Customer {
  id
}

fragment CustomerUpdate_customers on Customer {
  id
  firstName
  lastName
  email
  telephones
  birthDate
  companyId
  address {
    country
    latitude
    longitude
    locality
    postalCode
    region
    streetAddress
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "accessContact"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "count"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "cursor"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "customerCompanyId"
},
v4 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "direction"
},
v5 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "email"
},
v6 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "filters"
},
v7 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "firstName"
},
v8 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "lastName"
},
v9 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "locationIds"
},
v10 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "parent"
},
v11 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "phoneNumber"
},
v12 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "sortBy"
},
v13 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "tags"
},
v14 = {
  "kind": "Variable",
  "name": "accessContact",
  "variableName": "accessContact"
},
v15 = {
  "kind": "Variable",
  "name": "customerCompanyId",
  "variableName": "customerCompanyId"
},
v16 = {
  "kind": "Variable",
  "name": "direction",
  "variableName": "direction"
},
v17 = {
  "kind": "Variable",
  "name": "email",
  "variableName": "email"
},
v18 = {
  "kind": "Variable",
  "name": "filters",
  "variableName": "filters"
},
v19 = {
  "kind": "Variable",
  "name": "firstName",
  "variableName": "firstName"
},
v20 = {
  "kind": "Variable",
  "name": "lastName",
  "variableName": "lastName"
},
v21 = {
  "kind": "Variable",
  "name": "locationIds",
  "variableName": "locationIds"
},
v22 = {
  "kind": "Variable",
  "name": "parent",
  "variableName": "parent"
},
v23 = {
  "kind": "Variable",
  "name": "phoneNumber",
  "variableName": "phoneNumber"
},
v24 = {
  "kind": "Variable",
  "name": "sortBy",
  "variableName": "sortBy"
},
v25 = {
  "kind": "Variable",
  "name": "tags",
  "variableName": "tags"
},
v26 = [
  (v14/*: any*/),
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "cursor"
  },
  (v15/*: any*/),
  (v16/*: any*/),
  (v17/*: any*/),
  (v18/*: any*/),
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "count"
  },
  (v19/*: any*/),
  (v20/*: any*/),
  (v21/*: any*/),
  (v22/*: any*/),
  (v23/*: any*/),
  (v24/*: any*/),
  (v25/*: any*/)
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/),
      (v4/*: any*/),
      (v5/*: any*/),
      (v6/*: any*/),
      (v7/*: any*/),
      (v8/*: any*/),
      (v9/*: any*/),
      (v10/*: any*/),
      (v11/*: any*/),
      (v12/*: any*/),
      (v13/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "CustomerPageQuery",
    "selections": [
      {
        "args": [
          (v14/*: any*/),
          {
            "kind": "Variable",
            "name": "count",
            "variableName": "count"
          },
          {
            "kind": "Variable",
            "name": "cursor",
            "variableName": "cursor"
          },
          (v15/*: any*/),
          (v16/*: any*/),
          (v17/*: any*/),
          (v18/*: any*/),
          (v19/*: any*/),
          (v20/*: any*/),
          (v21/*: any*/),
          (v22/*: any*/),
          (v23/*: any*/),
          (v24/*: any*/),
          (v25/*: any*/)
        ],
        "kind": "FragmentSpread",
        "name": "CustomerMaster_customers"
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v2/*: any*/),
      (v10/*: any*/),
      (v6/*: any*/),
      (v9/*: any*/),
      (v0/*: any*/),
      (v5/*: any*/),
      (v7/*: any*/),
      (v8/*: any*/),
      (v11/*: any*/),
      (v13/*: any*/),
      (v3/*: any*/),
      (v12/*: any*/),
      (v4/*: any*/)
    ],
    "kind": "Operation",
    "name": "CustomerPageQuery",
    "selections": [
      {
        "alias": null,
        "args": (v26/*: any*/),
        "concreteType": "ListCustomerResponse",
        "kind": "LinkedField",
        "name": "customers",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "CustomerNode",
            "kind": "LinkedField",
            "name": "edges",
            "plural": true,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Customer",
                "kind": "LinkedField",
                "name": "node",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "id",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "firstName",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "lastName",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "email",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "telephones",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "createdOn",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "GalleryItem",
                    "kind": "LinkedField",
                    "name": "profileImage",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "largeImage",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "thumbImage",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "companyId",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Address",
                    "kind": "LinkedField",
                    "name": "address",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "country",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "locality",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "postalCode",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "region",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "streetAddress",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "latitude",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "longitude",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "birthDate",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "timezone",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "tag",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "preferredLanguage",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "__typename",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "cursor",
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "PageInfo",
            "kind": "LinkedField",
            "name": "pageInfo",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "endCursor",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "startCursor",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "hasNextPage",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "hasPreviousPage",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": (v26/*: any*/),
        "filters": [],
        "handle": "connection",
        "key": "CustomerMaster_customers",
        "kind": "LinkedHandle",
        "name": "customers"
      }
    ]
  },
  "params": {
    "cacheID": "a31db01d0ac851d1035322a463ebb4e2",
    "id": null,
    "metadata": {},
    "name": "CustomerPageQuery",
    "operationKind": "query",
    "text": "query CustomerPageQuery(\n  $count: Int!\n  $cursor: String\n  $parent: String\n  $filters: [ReportFilterInput]\n  $locationIds: [String]\n  $accessContact: Boolean\n  $email: String\n  $firstName: String\n  $lastName: String\n  $phoneNumber: String\n  $tags: [String]\n  $customerCompanyId: String\n  $sortBy: CustomerListSortBy\n  $direction: OrderBy\n) {\n  ...CustomerMaster_customers_3Tz1FU\n}\n\nfragment CustomerAvatar_customer on Customer {\n  id\n  firstName\n  profileImage {\n    largeImage\n    thumbImage\n  }\n}\n\nfragment CustomerDetail_customers on Customer {\n  id\n  firstName\n  lastName\n  email\n  profileImage {\n    largeImage\n    thumbImage\n  }\n  ...CustomerInfo_customer\n  ...CustomerStats_customer\n  ...CustomerAvatar_customer\n}\n\nfragment CustomerInfo_customer on Customer {\n  id\n  companyId\n  address {\n    country\n    locality\n    postalCode\n    region\n    streetAddress\n  }\n  birthDate\n  telephones\n  timezone\n  tag\n  preferredLanguage\n}\n\nfragment CustomerList_customers on Customer {\n  id\n  firstName\n  lastName\n  email\n  telephones\n  createdOn\n  ...CustomerAvatar_customer\n}\n\nfragment CustomerMaster_customers_3Tz1FU on Query {\n  customers(parent: $parent, first: $count, after: $cursor, locationIds: $locationIds, filters: $filters, accessContact: $accessContact, email: $email, firstName: $firstName, lastName: $lastName, customerCompanyId: $customerCompanyId, phoneNumber: $phoneNumber, tags: $tags, sortBy: $sortBy, direction: $direction) {\n    edges {\n      node {\n        ...CustomerList_customers\n        ...CustomerDetail_customers\n        ...CustomerUpdate_customers\n        id\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      startCursor\n      hasNextPage\n      hasPreviousPage\n    }\n  }\n}\n\nfragment CustomerStats_customer on Customer {\n  id\n}\n\nfragment CustomerUpdate_customers on Customer {\n  id\n  firstName\n  lastName\n  email\n  telephones\n  birthDate\n  companyId\n  address {\n    country\n    latitude\n    longitude\n    locality\n    postalCode\n    region\n    streetAddress\n  }\n}\n"
  }
};
})();
(node as any).hash = '0daea0143831725e11f7b78b412a73f1';
export default node;
