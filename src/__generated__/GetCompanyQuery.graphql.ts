/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type GetCompanyQueryVariables = {
    id?: string | null;
};
export type GetCompanyQueryResponse = {
    readonly company: {
        readonly id: string;
        readonly address: {
            readonly country: string;
            readonly region: string;
            readonly locality: string;
        } | null;
    };
};
export type GetCompanyQuery = {
    readonly response: GetCompanyQueryResponse;
    readonly variables: GetCompanyQueryVariables;
};



/*
query GetCompanyQuery(
  $id: ID
) {
  company(id: $id) {
    id
    address {
      country
      region
      locality
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "id",
        "variableName": "id"
      }
    ],
    "concreteType": "Company",
    "kind": "LinkedField",
    "name": "company",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Address",
        "kind": "LinkedField",
        "name": "address",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "country",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "region",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "locality",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "GetCompanyQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "GetCompanyQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "4a55bc0d50c7a90e541859cd4a84e5f2",
    "id": null,
    "metadata": {},
    "name": "GetCompanyQuery",
    "operationKind": "query",
    "text": "query GetCompanyQuery(\n  $id: ID\n) {\n  company(id: $id) {\n    id\n    address {\n      country\n      region\n      locality\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = 'a877a793b67722ef16980849f5a91977';
export default node;
