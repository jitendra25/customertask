/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type GetCustomerQueryVariables = {
    id?: string | null;
};
export type GetCustomerQueryResponse = {
    readonly customer: {
        readonly id: string;
    };
};
export type GetCustomerQuery = {
    readonly response: GetCustomerQueryResponse;
    readonly variables: GetCustomerQueryVariables;
};



/*
query GetCustomerQuery(
  $id: ID
) {
  customer(id: $id) {
    id
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "id",
        "variableName": "id"
      }
    ],
    "concreteType": "Customer",
    "kind": "LinkedField",
    "name": "customer",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "GetCustomerQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "GetCustomerQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "970bacc886429cfaf88c0072f25817af",
    "id": null,
    "metadata": {},
    "name": "GetCustomerQuery",
    "operationKind": "query",
    "text": "query GetCustomerQuery(\n  $id: ID\n) {\n  customer(id: $id) {\n    id\n  }\n}\n"
  }
};
})();
(node as any).hash = 'ed114cc5fa1353120d6cba47a3880ae0';
export default node;
