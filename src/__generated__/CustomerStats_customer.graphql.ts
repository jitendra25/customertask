/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type CustomerStats_customer = {
    readonly id: string;
    readonly " $refType": "CustomerStats_customer";
};
export type CustomerStats_customer$data = CustomerStats_customer;
export type CustomerStats_customer$key = {
    readonly " $data"?: CustomerStats_customer$data;
    readonly " $fragmentRefs": FragmentRefs<"CustomerStats_customer">;
};



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "CustomerStats_customer",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "Customer",
  "abstractKey": null
};
(node as any).hash = '93c16caa1f7838e5bda043a94cb796b4';
export default node;
