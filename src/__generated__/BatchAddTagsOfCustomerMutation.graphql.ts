/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type BatchAddTagsOfCustomerInput = {
    clientMutationId?: string | null;
    ids?: Array<string | null> | null;
    tags?: Array<string | null> | null;
};
export type BatchAddTagsOfCustomerMutationVariables = {
    input?: BatchAddTagsOfCustomerInput | null;
};
export type BatchAddTagsOfCustomerMutationResponse = {
    readonly batchAddTagsOfCustomer: {
        readonly clientMutationId: string;
        readonly payload: {};
    };
};
export type BatchAddTagsOfCustomerMutation = {
    readonly response: BatchAddTagsOfCustomerMutationResponse;
    readonly variables: BatchAddTagsOfCustomerMutationVariables;
};



/*
mutation BatchAddTagsOfCustomerMutation(
  $input: BatchAddTagsOfCustomerInput
) {
  batchAddTagsOfCustomer(input: $input) {
    clientMutationId
    payload
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "BatchAddTagsOfCustomerPayload",
    "kind": "LinkedField",
    "name": "batchAddTagsOfCustomer",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "clientMutationId",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "payload",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "BatchAddTagsOfCustomerMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "BatchAddTagsOfCustomerMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "bf47b051921d1de8a4c483024b065028",
    "id": null,
    "metadata": {},
    "name": "BatchAddTagsOfCustomerMutation",
    "operationKind": "mutation",
    "text": "mutation BatchAddTagsOfCustomerMutation(\n  $input: BatchAddTagsOfCustomerInput\n) {\n  batchAddTagsOfCustomer(input: $input) {\n    clientMutationId\n    payload\n  }\n}\n"
  }
};
})();
(node as any).hash = 'eb7fe6234bbb866c9ee7e86edb61d200';
export default node;
