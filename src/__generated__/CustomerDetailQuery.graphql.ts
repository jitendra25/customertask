/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type CustomerDetailQueryVariables = {
    customerId?: string | null;
};
export type CustomerDetailQueryResponse = {
    readonly customerLocationLinks: {
        readonly locationIds: ReadonlyArray<string>;
    };
};
export type CustomerDetailQuery = {
    readonly response: CustomerDetailQueryResponse;
    readonly variables: CustomerDetailQueryVariables;
};



/*
query CustomerDetailQuery(
  $customerId: String
) {
  customerLocationLinks(customerId: $customerId) {
    locationIds
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "customerId"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "customerId",
        "variableName": "customerId"
      }
    ],
    "concreteType": "GetCustomerLocationLinksResponse",
    "kind": "LinkedField",
    "name": "customerLocationLinks",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "locationIds",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "CustomerDetailQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "CustomerDetailQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "0ad712299d7a358373d480e8b93748e3",
    "id": null,
    "metadata": {},
    "name": "CustomerDetailQuery",
    "operationKind": "query",
    "text": "query CustomerDetailQuery(\n  $customerId: String\n) {\n  customerLocationLinks(customerId: $customerId) {\n    locationIds\n  }\n}\n"
  }
};
})();
(node as any).hash = '89f6cd8f89f19e2ca43c336edc6b2f64';
export default node;
