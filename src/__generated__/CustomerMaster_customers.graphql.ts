/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type CustomerMaster_customers = {
    readonly customers: {
        readonly edges: ReadonlyArray<{
            readonly node: {
                readonly id: string;
                readonly " $fragmentRefs": FragmentRefs<"CustomerList_customers" | "CustomerDetail_customers" | "CustomerUpdate_customers">;
            } | null;
            readonly cursor: string;
        }>;
        readonly pageInfo: {
            readonly endCursor: string;
            readonly startCursor: string;
            readonly hasNextPage: boolean;
            readonly hasPreviousPage: boolean;
        } | null;
    };
    readonly " $refType": "CustomerMaster_customers";
};
export type CustomerMaster_customers$data = CustomerMaster_customers;
export type CustomerMaster_customers$key = {
    readonly " $data"?: CustomerMaster_customers$data;
    readonly " $fragmentRefs": FragmentRefs<"CustomerMaster_customers">;
};



const node: ReaderFragment = {
  "argumentDefinitions": [
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "accessContact"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "count"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "cursor"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "customerCompanyId"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "direction"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "email"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "filters"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "firstName"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "lastName"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "locationIds"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "parent"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "phoneNumber"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "sortBy"
    },
    {
      "defaultValue": null,
      "kind": "LocalArgument",
      "name": "tags"
    }
  ],
  "kind": "Fragment",
  "metadata": {
    "connection": [
      {
        "count": "count",
        "cursor": "cursor",
        "direction": "forward",
        "path": [
          "customers"
        ]
      }
    ]
  },
  "name": "CustomerMaster_customers",
  "selections": [
    {
      "alias": "customers",
      "args": null,
      "concreteType": "ListCustomerResponse",
      "kind": "LinkedField",
      "name": "__CustomerMaster_customers_connection",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "CustomerNode",
          "kind": "LinkedField",
          "name": "edges",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "Customer",
              "kind": "LinkedField",
              "name": "node",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "id",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "__typename",
                  "storageKey": null
                },
                {
                  "args": null,
                  "kind": "FragmentSpread",
                  "name": "CustomerList_customers"
                },
                {
                  "args": null,
                  "kind": "FragmentSpread",
                  "name": "CustomerDetail_customers"
                },
                {
                  "args": null,
                  "kind": "FragmentSpread",
                  "name": "CustomerUpdate_customers"
                }
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "cursor",
              "storageKey": null
            }
          ],
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "PageInfo",
          "kind": "LinkedField",
          "name": "pageInfo",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "endCursor",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "startCursor",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "hasNextPage",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "hasPreviousPage",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Query",
  "abstractKey": null
};
(node as any).hash = 'cb76cb3c365d2e4dbae352d8a0d150fd';
export default node;
