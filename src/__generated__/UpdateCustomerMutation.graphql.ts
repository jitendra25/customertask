/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type Gender = "FEMALE" | "MALE" | "UNSPECIFIED" | "%future added value";
export type UpdateCustomerInput = {
    clientMutationId?: string | null;
    customer?: CustomerInput | null;
    locationIds?: Array<string | null> | null;
    updateMask?: FieldMaskInput | null;
};
export type CustomerInput = {
    address?: AddressInput | null;
    birthDate?: string | null;
    campaign?: string | null;
    companyId?: string | null;
    createdBy?: string | null;
    createdOn?: string | null;
    customerLevel?: string | null;
    email?: string | null;
    firstName?: string | null;
    gender?: Gender | null;
    id?: string | null;
    invitedBy?: string | null;
    isInvitedByAdmin?: boolean | null;
    lastName?: string | null;
    medium?: string | null;
    metadata?: string | null;
    preferredLanguage?: string | null;
    profileImage?: GalleryItemInput | null;
    source?: string | null;
    ssoId?: string | null;
    tag?: Array<string | null> | null;
    telephones?: Array<string | null> | null;
    timezone?: string | null;
    updatedBy?: string | null;
    updatedOn?: string | null;
    userId?: string | null;
};
export type AddressInput = {
    country?: string | null;
    latitude?: number | null;
    locality?: string | null;
    longitude?: number | null;
    postalCode?: string | null;
    region?: string | null;
    streetAddress?: string | null;
};
export type GalleryItemInput = {
    largeImage?: string | null;
    thumbImage?: string | null;
};
export type FieldMaskInput = {
    paths?: Array<string | null> | null;
};
export type UpdateCustomerUsingLocationRightsInput = {
    clientMutationId?: string | null;
    customer?: CustomerInput | null;
    updateMask?: FieldMaskInput | null;
};
export type UpdateCustomerMutationVariables = {
    input?: UpdateCustomerInput | null;
    input2?: UpdateCustomerUsingLocationRightsInput | null;
    companyLevel: boolean;
};
export type UpdateCustomerMutationResponse = {
    readonly updateCustomer?: {
        readonly clientMutationId: string;
        readonly payload: {
            readonly address: {
                readonly country: string;
                readonly locality: string;
                readonly postalCode: string;
                readonly region: string;
                readonly streetAddress: string;
            } | null;
            readonly birthDate: string | null;
            readonly createdBy: string;
            readonly createdOn: string | null;
            readonly email: string;
            readonly firstName: string;
            readonly gender: Gender;
            readonly id: string;
            readonly lastName: string;
            readonly metadata: string | null;
            readonly preferredLanguage: string;
            readonly profileImage: {
                readonly largeImage: string;
                readonly thumbImage: string;
            } | null;
            readonly tag: ReadonlyArray<string>;
            readonly telephones: ReadonlyArray<string>;
            readonly timezone: string;
            readonly updatedBy: string;
            readonly updatedOn: string | null;
            readonly userId: string;
            readonly companyId: string;
        };
    };
    readonly updateCustomerUsingLocationRights?: {
        readonly payload: {
            readonly address: {
                readonly country: string;
                readonly locality: string;
                readonly postalCode: string;
                readonly region: string;
                readonly streetAddress: string;
            } | null;
            readonly birthDate: string | null;
            readonly createdBy: string;
            readonly createdOn: string | null;
            readonly email: string;
            readonly firstName: string;
            readonly gender: Gender;
            readonly id: string;
            readonly lastName: string;
            readonly metadata: string | null;
            readonly preferredLanguage: string;
            readonly profileImage: {
                readonly largeImage: string;
                readonly thumbImage: string;
            } | null;
            readonly tag: ReadonlyArray<string>;
            readonly telephones: ReadonlyArray<string>;
            readonly timezone: string;
            readonly updatedBy: string;
            readonly updatedOn: string | null;
            readonly userId: string;
            readonly companyId: string;
        };
    };
};
export type UpdateCustomerMutation = {
    readonly response: UpdateCustomerMutationResponse;
    readonly variables: UpdateCustomerMutationVariables;
};



/*
mutation UpdateCustomerMutation(
  $input: UpdateCustomerInput
  $input2: UpdateCustomerUsingLocationRightsInput
  $companyLevel: Boolean!
) {
  updateCustomer(input: $input) @include(if: $companyLevel) {
    clientMutationId
    payload {
      address {
        country
        locality
        postalCode
        region
        streetAddress
      }
      birthDate
      createdBy
      createdOn
      email
      firstName
      gender
      id
      lastName
      metadata
      preferredLanguage
      profileImage {
        largeImage
        thumbImage
      }
      tag
      telephones
      timezone
      updatedBy
      updatedOn
      userId
      companyId
    }
  }
  updateCustomerUsingLocationRights(input: $input2) @skip(if: $companyLevel) {
    payload {
      address {
        country
        locality
        postalCode
        region
        streetAddress
      }
      birthDate
      createdBy
      createdOn
      email
      firstName
      gender
      id
      lastName
      metadata
      preferredLanguage
      profileImage {
        largeImage
        thumbImage
      }
      tag
      telephones
      timezone
      updatedBy
      updatedOn
      userId
      companyId
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "companyLevel"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "input"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "input2"
},
v3 = {
  "alias": null,
  "args": null,
  "concreteType": "Customer",
  "kind": "LinkedField",
  "name": "payload",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "Address",
      "kind": "LinkedField",
      "name": "address",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "country",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "locality",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "postalCode",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "region",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "streetAddress",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "birthDate",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdOn",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "email",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "firstName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "gender",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "lastName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "metadata",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "preferredLanguage",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "GalleryItem",
      "kind": "LinkedField",
      "name": "profileImage",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "largeImage",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "thumbImage",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "tag",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "telephones",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "timezone",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "updatedBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "updatedOn",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "userId",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "companyId",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v4 = [
  {
    "condition": "companyLevel",
    "kind": "Condition",
    "passingValue": true,
    "selections": [
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "input",
            "variableName": "input"
          }
        ],
        "concreteType": "UpdateCustomerPayload",
        "kind": "LinkedField",
        "name": "updateCustomer",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "clientMutationId",
            "storageKey": null
          },
          (v3/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  {
    "condition": "companyLevel",
    "kind": "Condition",
    "passingValue": false,
    "selections": [
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "input",
            "variableName": "input2"
          }
        ],
        "concreteType": "UpdateCustomerUsingLocationRightsPayload",
        "kind": "LinkedField",
        "name": "updateCustomerUsingLocationRights",
        "plural": false,
        "selections": [
          (v3/*: any*/)
        ],
        "storageKey": null
      }
    ]
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "UpdateCustomerMutation",
    "selections": (v4/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v2/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "UpdateCustomerMutation",
    "selections": (v4/*: any*/)
  },
  "params": {
    "cacheID": "0e3c7ea57b70c9e5595eb7549c723394",
    "id": null,
    "metadata": {},
    "name": "UpdateCustomerMutation",
    "operationKind": "mutation",
    "text": "mutation UpdateCustomerMutation(\n  $input: UpdateCustomerInput\n  $input2: UpdateCustomerUsingLocationRightsInput\n  $companyLevel: Boolean!\n) {\n  updateCustomer(input: $input) @include(if: $companyLevel) {\n    clientMutationId\n    payload {\n      address {\n        country\n        locality\n        postalCode\n        region\n        streetAddress\n      }\n      birthDate\n      createdBy\n      createdOn\n      email\n      firstName\n      gender\n      id\n      lastName\n      metadata\n      preferredLanguage\n      profileImage {\n        largeImage\n        thumbImage\n      }\n      tag\n      telephones\n      timezone\n      updatedBy\n      updatedOn\n      userId\n      companyId\n    }\n  }\n  updateCustomerUsingLocationRights(input: $input2) @skip(if: $companyLevel) {\n    payload {\n      address {\n        country\n        locality\n        postalCode\n        region\n        streetAddress\n      }\n      birthDate\n      createdBy\n      createdOn\n      email\n      firstName\n      gender\n      id\n      lastName\n      metadata\n      preferredLanguage\n      profileImage {\n        largeImage\n        thumbImage\n      }\n      tag\n      telephones\n      timezone\n      updatedBy\n      updatedOn\n      userId\n      companyId\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = 'e5845dcd528ee34ab2dc065a310f95a4';
export default node;
