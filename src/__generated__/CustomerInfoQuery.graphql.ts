/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type ActivityType = "ADDED_ON" | "LAST_ACTIVITY_ON" | "LAST_APPOINTMENT_BOOKED_ON" | "LAST_BOOKED_ON" | "LAST_CLASS_BOOKED_ON" | "LAST_PURCHASED_ON" | "LAST_REVIEWED_ON" | "UNSPECIFIED_ACTIVITY_TYPE" | "%future added value";
export type CustomerInfoQueryVariables = {
    parent?: string | null;
    customerId?: string | null;
    note: boolean;
    activity: boolean;
};
export type CustomerInfoQueryResponse = {
    readonly customerActivities?: {
        readonly customerActivities: ReadonlyArray<{
            readonly activityType: ActivityType;
            readonly date: string | null;
            readonly appName: string;
        }>;
    };
    readonly notes?: {
        readonly edges: ReadonlyArray<{
            readonly node: {
                readonly id: string;
                readonly description: string;
                readonly typeId: string;
                readonly private: boolean;
                readonly appTypeName: string;
            } | null;
        }>;
    };
};
export type CustomerInfoQuery = {
    readonly response: CustomerInfoQueryResponse;
    readonly variables: CustomerInfoQueryVariables;
};



/*
query CustomerInfoQuery(
  $parent: String
  $customerId: String
  $note: Boolean!
  $activity: Boolean!
) {
  customerActivities(customerId: $customerId, parent: $parent) @include(if: $activity) {
    customerActivities {
      activityType
      date
      appName
    }
  }
  notes(first: 1, parent: $parent, against: $customerId, statusType: PRIVATE, getPrivate: true, allStaff: false, appTypeName: ["Customers"]) @include(if: $note) {
    edges {
      node {
        id
        description
        typeId
        private
        appTypeName
      }
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "activity"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "customerId"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "note"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "parent"
},
v4 = {
  "kind": "Variable",
  "name": "parent",
  "variableName": "parent"
},
v5 = [
  {
    "condition": "activity",
    "kind": "Condition",
    "passingValue": true,
    "selections": [
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "customerId",
            "variableName": "customerId"
          },
          (v4/*: any*/)
        ],
        "concreteType": "GetCustomerActivitiesResponse",
        "kind": "LinkedField",
        "name": "customerActivities",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "CustomerActivity",
            "kind": "LinkedField",
            "name": "customerActivities",
            "plural": true,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "activityType",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "date",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "appName",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  {
    "condition": "note",
    "kind": "Condition",
    "passingValue": true,
    "selections": [
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "against",
            "variableName": "customerId"
          },
          {
            "kind": "Literal",
            "name": "allStaff",
            "value": false
          },
          {
            "kind": "Literal",
            "name": "appTypeName",
            "value": [
              "Customers"
            ]
          },
          {
            "kind": "Literal",
            "name": "first",
            "value": 1
          },
          {
            "kind": "Literal",
            "name": "getPrivate",
            "value": true
          },
          (v4/*: any*/),
          {
            "kind": "Literal",
            "name": "statusType",
            "value": "PRIVATE"
          }
        ],
        "concreteType": "ListNoteResponse",
        "kind": "LinkedField",
        "name": "notes",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "NoteNode",
            "kind": "LinkedField",
            "name": "edges",
            "plural": true,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Note",
                "kind": "LinkedField",
                "name": "node",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "id",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "description",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "typeId",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "private",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "appTypeName",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "CustomerInfoQuery",
    "selections": (v5/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v3/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "CustomerInfoQuery",
    "selections": (v5/*: any*/)
  },
  "params": {
    "cacheID": "227a11709a4a6b276d98510aed38ea04",
    "id": null,
    "metadata": {},
    "name": "CustomerInfoQuery",
    "operationKind": "query",
    "text": "query CustomerInfoQuery(\n  $parent: String\n  $customerId: String\n  $note: Boolean!\n  $activity: Boolean!\n) {\n  customerActivities(customerId: $customerId, parent: $parent) @include(if: $activity) {\n    customerActivities {\n      activityType\n      date\n      appName\n    }\n  }\n  notes(first: 1, parent: $parent, against: $customerId, statusType: PRIVATE, getPrivate: true, allStaff: false, appTypeName: [\"Customers\"]) @include(if: $note) {\n    edges {\n      node {\n        id\n        description\n        typeId\n        private\n        appTypeName\n      }\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = 'a7de485d853b02f4fcdd53a31f3bbcae';
export default node;
