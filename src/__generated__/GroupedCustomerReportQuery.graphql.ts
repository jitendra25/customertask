/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type DateslotInput = {
    endTime?: string | null;
    startTime?: string | null;
};
export type GroupedCustomerReportQueryVariables = {
    dateRange?: DateslotInput | null;
    groupByMonth?: boolean | null;
    limit?: number | null;
    locationIds?: Array<string | null> | null;
    offset?: number | null;
    parent?: string | null;
    timezone?: string | null;
};
export type GroupedCustomerReportQueryResponse = {
    readonly groupedCustomerSingupReport: {
        readonly edges: ReadonlyArray<{
            readonly node: {
                readonly signupOn: string | null;
                readonly customerCount: number;
            } | null;
            readonly cursor: string;
        }>;
        readonly pageInfo: {
            readonly nextOffset: number;
            readonly hasPreviousPage: boolean;
            readonly previousOffset: number;
            readonly hasNextPage: boolean;
        } | null;
    };
};
export type GroupedCustomerReportQuery = {
    readonly response: GroupedCustomerReportQueryResponse;
    readonly variables: GroupedCustomerReportQueryVariables;
};



/*
query GroupedCustomerReportQuery(
  $dateRange: DateslotInput
  $groupByMonth: Boolean
  $limit: Int
  $locationIds: [String]
  $offset: Int
  $parent: String
  $timezone: String
) {
  groupedCustomerSingupReport(dateRange: $dateRange, groupByMonth: $groupByMonth, limit: $limit, locationIds: $locationIds, offset: $offset, parent: $parent, timezone: $timezone) {
    edges {
      node {
        signupOn
        customerCount
      }
      cursor
    }
    pageInfo {
      nextOffset
      hasPreviousPage
      previousOffset
      hasNextPage
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "dateRange"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "groupByMonth"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "limit"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "locationIds"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "offset"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "parent"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "timezone"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "dateRange",
        "variableName": "dateRange"
      },
      {
        "kind": "Variable",
        "name": "groupByMonth",
        "variableName": "groupByMonth"
      },
      {
        "kind": "Variable",
        "name": "limit",
        "variableName": "limit"
      },
      {
        "kind": "Variable",
        "name": "locationIds",
        "variableName": "locationIds"
      },
      {
        "kind": "Variable",
        "name": "offset",
        "variableName": "offset"
      },
      {
        "kind": "Variable",
        "name": "parent",
        "variableName": "parent"
      },
      {
        "kind": "Variable",
        "name": "timezone",
        "variableName": "timezone"
      }
    ],
    "concreteType": "GetGroupedCustomerSignupReportResponse",
    "kind": "LinkedField",
    "name": "groupedCustomerSingupReport",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "GroupedCustomerSignupReportNode",
        "kind": "LinkedField",
        "name": "edges",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "GroupedCustomerSignupReportObject",
            "kind": "LinkedField",
            "name": "node",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "signupOn",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "customerCount",
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "cursor",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "PaginationInfo",
        "kind": "LinkedField",
        "name": "pageInfo",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "nextOffset",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "hasPreviousPage",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "previousOffset",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "hasNextPage",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "GroupedCustomerReportQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "GroupedCustomerReportQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "8a43cf11d1add0ac63417399a3099995",
    "id": null,
    "metadata": {},
    "name": "GroupedCustomerReportQuery",
    "operationKind": "query",
    "text": "query GroupedCustomerReportQuery(\n  $dateRange: DateslotInput\n  $groupByMonth: Boolean\n  $limit: Int\n  $locationIds: [String]\n  $offset: Int\n  $parent: String\n  $timezone: String\n) {\n  groupedCustomerSingupReport(dateRange: $dateRange, groupByMonth: $groupByMonth, limit: $limit, locationIds: $locationIds, offset: $offset, parent: $parent, timezone: $timezone) {\n    edges {\n      node {\n        signupOn\n        customerCount\n      }\n      cursor\n    }\n    pageInfo {\n      nextOffset\n      hasPreviousPage\n      previousOffset\n      hasNextPage\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = '932a9907d986b87de65823c852b193e5';
export default node;
