/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type ListCustomerQueryVariables = {
    parent?: string | null;
    count?: number | null;
    cursor?: string | null;
};
export type ListCustomerQueryResponse = {
    readonly customers: {
        readonly edges: ReadonlyArray<{
            readonly node: {
                readonly id: string;
                readonly firstName: string;
                readonly lastName: string;
                readonly email: string;
                readonly preferredLanguage: string;
                readonly timezone: string;
                readonly profileImage: {
                    readonly thumbImage: string;
                } | null;
            } | null;
        }>;
    };
};
export type ListCustomerQuery = {
    readonly response: ListCustomerQueryResponse;
    readonly variables: ListCustomerQueryVariables;
};



/*
query ListCustomerQuery(
  $parent: String
  $count: Int
  $cursor: String
) {
  customers(parent: $parent, first: $count, after: $cursor) {
    edges {
      node {
        id
        firstName
        lastName
        email
        preferredLanguage
        timezone
        profileImage {
          thumbImage
        }
      }
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "count"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "cursor"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "parent"
},
v3 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "after",
        "variableName": "cursor"
      },
      {
        "kind": "Variable",
        "name": "first",
        "variableName": "count"
      },
      {
        "kind": "Variable",
        "name": "parent",
        "variableName": "parent"
      }
    ],
    "concreteType": "ListCustomerResponse",
    "kind": "LinkedField",
    "name": "customers",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "CustomerNode",
        "kind": "LinkedField",
        "name": "edges",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Customer",
            "kind": "LinkedField",
            "name": "node",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "firstName",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "lastName",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "email",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "preferredLanguage",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "timezone",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "GalleryItem",
                "kind": "LinkedField",
                "name": "profileImage",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "thumbImage",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "ListCustomerQuery",
    "selections": (v3/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v2/*: any*/),
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Operation",
    "name": "ListCustomerQuery",
    "selections": (v3/*: any*/)
  },
  "params": {
    "cacheID": "5b150000fdedc867f225d33c77b02709",
    "id": null,
    "metadata": {},
    "name": "ListCustomerQuery",
    "operationKind": "query",
    "text": "query ListCustomerQuery(\n  $parent: String\n  $count: Int\n  $cursor: String\n) {\n  customers(parent: $parent, first: $count, after: $cursor) {\n    edges {\n      node {\n        id\n        firstName\n        lastName\n        email\n        preferredLanguage\n        timezone\n        profileImage {\n          thumbImage\n        }\n      }\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = '87bc09c3fac8f5016a2804ddfe940201';
export default node;
