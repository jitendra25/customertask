import { PubSub } from '@saastack/pubsub'
import namespace from './namespace'
import React from 'react'
import hooks from '@saastack/hooks'
import {Trans } from '@lingui/macro'
import loadable from '@loadable/component'

PubSub.register(Object.values(namespace))
const NotePage = loadable(() => import('@saastack/notes/pages/NotePage'))
let loaded = false

const load = () => {
    if (loaded) {
        return
    }
    hooks.customer.tab.registerHook('notes', {
        component: NotePage,
        label: <Trans>Notes</Trans>,
    })
    loaded = true
}

load()
