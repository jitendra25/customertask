import { commitMutation, graphql, Variables } from 'react-relay'
import {
    ConnectionHandler,
    Disposable,
    Environment,
    RecordProxy,
    RecordSourceSelectorProxy,
} from 'relay-runtime'
import {
    CreateCustomerInput,
    CreateCustomerMutation,
    CreateCustomerMutationResponse,
    CustomerInput,
    CustomerSendNotificationInput,
} from '../__generated__/CreateCustomerMutation.graphql'

interface Callbacks {
    onError?: (message: string) => void
    onSuccess?: (response: CustomerInput) => void
}

const mutation = graphql`
    mutation CreateCustomerMutation($input: CreateCustomerInput) {
        createCustomer(input: $input) {
            clientMutationId
            payload {
                id
                firstName
                lastName
                email
            }
        }
    }
`

let tempID = 0

const sharedUpdater = (store: RecordSourceSelectorProxy, node: RecordProxy) => {
  const rootProxy = store.getRoot()
    const connection = ConnectionHandler.getConnection(rootProxy, 'CustomerMaster_customers')
    if (connection) {
        const newEdge = ConnectionHandler.createEdge(store, connection, node,'CustomerNode')
        ConnectionHandler.insertEdgeBefore(connection, newEdge)
    }
}

const commit = (
    environment: Environment,
    variables: Variables,
    customer: CustomerInput,
    sendNotification: CustomerSendNotificationInput,
    locationIds: [string],
    callbacks?: Callbacks
): Disposable => {
    const input: CreateCustomerInput = {
        parent: variables.parent,
        customer,
        locationIds,
        sendNotification,
        clientMutationId: `${tempID++}`,
    }

    return commitMutation<CreateCustomerMutation>(environment, {
        mutation,
        variables: {
            input,
        },
        updater: (store: RecordSourceSelectorProxy) => {
            const payload = store.getRootField('createCustomer')
            const node = payload!.getLinkedRecord('payload')
            sharedUpdater(store, node!)
        },
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: (response: CreateCustomerMutationResponse) => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess({ ...customer, ...response.createCustomer.payload })
            }
        },
    })
}

export default { commit }
