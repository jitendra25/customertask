import { MutationCallbacks, setNodeValue } from '@saastack/relay'
import { pick } from 'lodash-es'
import { commitMutation, graphql } from 'react-relay'
import { Disposable, Environment, RecordSourceSelectorProxy } from 'relay-runtime'
import {
    CustomerInput,
    UpdateCustomerInput,
    UpdateCustomerUsingLocationRightsInput,
    UpdateCustomerMutation,
    UpdateCustomerMutationResponse,
} from '../__generated__/UpdateCustomerMutation.graphql'

const mutation = graphql`
    mutation UpdateCustomerMutation(
        $input: UpdateCustomerInput
        $input2: UpdateCustomerUsingLocationRightsInput
        $companyLevel: Boolean!
    ) {
        updateCustomer(input: $input) @include(if: $companyLevel) {
            clientMutationId
            payload {
                address {
                    country
                    locality
                    postalCode
                    region
                    streetAddress
                }
                birthDate
                createdBy
                createdOn
                email
                firstName
                gender
                id
                lastName
                metadata
                preferredLanguage
                profileImage {
                    largeImage
                    thumbImage
                }
                tag
                telephones
                timezone
                updatedBy
                updatedOn
                userId
                companyId
            }
        }
        updateCustomerUsingLocationRights(input: $input2) @skip(if: $companyLevel) {
           payload {
                address {
                    country
                    locality
                    postalCode
                    region
                    streetAddress
                }
                birthDate
                createdBy
                createdOn
                email
                firstName
                gender
                id
                lastName
                metadata
                preferredLanguage
                profileImage {
                    largeImage
                    thumbImage
                }
                tag
                telephones
                timezone
                updatedBy
                updatedOn
                userId
                companyId
            }
        }
    }
`

let tempID = 0

const sharedUpdater = (
    store: RecordSourceSelectorProxy,
    customer: CustomerInput,
    updateMask: string[]
) => {
    if (customer.id) {
        const node = store.get(customer.id)
        if (node) {
            setNodeValue(store as any, node, pick(customer, updateMask))
        }
    }
}

const commit = (
    environment: Environment,
    customer: CustomerInput,
    updateMask: string[],
    companyLevel: boolean,
    callbacks?: MutationCallbacks<CustomerInput>,
    locationIds?: string[]
): Disposable => {
    const input: UpdateCustomerInput | UpdateCustomerUsingLocationRightsInput = {
        updateMask: { paths: updateMask },
        customer,
        locationIds,
        clientMutationId: `${tempID++}`,
    }
    return commitMutation<UpdateCustomerMutation>(environment, {
        mutation,
        variables: {
            input,
            input2: input,
            companyLevel,
        },
        optimisticUpdater: (store: RecordSourceSelectorProxy) =>
            sharedUpdater(store, customer, updateMask),
        updater:(store:RecordSourceSelectorProxy)=>{
           sharedUpdater(store,customer,updateMask)
        }
,
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: (response: UpdateCustomerMutationResponse) => {
            if (callbacks && callbacks.onSuccess) {
                const payload = (
                    response.updateCustomer || response.updateCustomerUsingLocationRights
                )?.payload as any
                callbacks.onSuccess({ ...customer, ...payload })
            }
        },
    })
}

export default { commit }
