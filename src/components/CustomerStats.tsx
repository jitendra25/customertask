import { t, Trans } from '@lingui/macro'
import { Box, Theme, Typography } from '@material-ui/core'
import { Loading } from '@saastack/components'
import { useConfig } from '@saastack/core'
import { useI18n } from '@saastack/i18n'
import { useQuery } from '@saastack/relay'
import { formatCurrency, useDidMountEffect } from '@saastack/utils'
import moment from 'moment'
import React from 'react'
import ReactApexChart from 'react-apexcharts'
import { createFragmentContainer, graphql } from 'react-relay'
import { CustomerStats_customer } from '../__generated__/CustomerStats_customer.graphql'
import { CustomerStatsQuery } from '../__generated__/CustomerStatsQuery.graphql'
import InfoCard from './InfoCard'
import { useTheme } from '@material-ui/styles'

interface Props {
    parent: string
    customer: CustomerStats_customer
}

const query = graphql`
    query CustomerStatsQuery(
        $customerId: String
        $startDate: Timestamp
        $endDate: Timestamp
        $timezone: String
        $parent: String
    ) {
        customerAmounts(customerId: $customerId, parent: $parent) {
            customerAmounts {
                amount {
                    amount
                    currency
                }
                amountType
                appName
            }
        }
        customerNumbers(customerId: $customerId, parent: $parent) {
            customerNumbers {
                number
                numberType
                appName
            }
        }
        salesReportByCustomer(
            customerId: $customerId
            startDate: $startDate
            endDate: $endDate
            timezone: $timezone
            parent: $parent
        ) {
            data {
                date
                sales {
                    amount
                    currency
                }
            }
        }
    }
`

const labelMapping: Record<string, React.ReactNode> = {
    ARPV: <Trans>Avg revenue per visit</Trans>,
    LTV: <Trans>Lifetime value</Trans>,
    TOTAL_VISITS: <Trans>Bookings</Trans>,
    TOTAL_APPOINTMENTS: <Trans>Appointments purchased</Trans>,
    TOTAL_CLASSES: <Trans>Classes purchased</Trans>,
    TOTAL_GIFT_CERTIFICATES: <Trans>Gift certificates purchased</Trans>,
    TOTAL_MEMBERSHIPS: <Trans>Memberships purchased</Trans>,
    TOTAL_PACKAGES: <Trans>Packages purchased</Trans>,
    TOTAL_APPOINTMENT_AMOUNT: <Trans>Appointments revenue</Trans>,
    TOTAL_CLASS_AMOUNT: <Trans>Classes revenue</Trans>,
    TOTAL_GIFT_CERTIFICATE_AMOUNT: <Trans>Gift certificates revenue</Trans>,
    TOTAL_MEMBERSHIP_AMOUNT: <Trans>Memberships revenue</Trans>,
    TOTAL_PACKAGE_AMOUNT: <Trans>Packages revenue</Trans>,
}

const formatPrice = (e: any) => {
    if (e.amount) {
        return formatCurrency(e.amount, e.currency)
    }
    return '-'
}

const formatNumber = (e: any) => {
    if (e) {
        return new Intl.NumberFormat(undefined, {
            notation: 'compact',
        } as any).format(e)
    }
    return '-'
}

const startDate = moment().startOf('y').toISOString()
const endDate = moment().endOf('y').toISOString()

const CustomerStats: React.FC<Props> = ({ customer, parent }) => {
    const { palette } = useTheme<Theme>()
    const { timezone, currency: defaultCurrency, activeApps } = useConfig()
    const { i18n } = useI18n()
    const variables: CustomerStatsQuery['variables'] = {
        customerId: customer?.id,
        startDate,
        endDate,
        timezone,
        parent,
    }
    const { data, refetch, loading } = useQuery<CustomerStatsQuery>(query, variables)
    useDidMountEffect(() => {
        refetch()
    }, [customer?.id, parent])
    const salesData = Array(12).fill(0)
    const salesDataMap: Record<number, { amount: number; currency: string }> = {}
    let currency = defaultCurrency
    data?.salesReportByCustomer.data.map((dt) => {
        const index = moment(dt.date).month()
        salesDataMap[index] = dt.sales!
        salesData[index] = dt.sales?.amount
        currency = dt.sales?.currency!
    })
    const chartData = {
        series: [
            {
                name: i18n._(t`Amount`),
                data: salesData,
            },
        ],
        options: {
            theme: {
                mode: palette.type,
            },
            chart: {
                height: 350,
                type: 'bar',
                toolbar: {
                    show: false,
                },
            },
            plotOptions: {
                bar: {
                    columnWidth: '50%',
                    endingShape: 'rounded',
                },
            },
            dataLabels: {
                enabled: false,
            },
            stroke: {
                width: 2,
            },
            grid: {
                row: {
                    colors: ['#fff', '#f2f2f2'],
                },
            },
            yaxis: {
                labels: {
                    formatter: (value: number) => formatCurrency(value, currency),
                },
            },
            xaxis: {
                categories: moment.monthsShort(),
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'light',
                    type: 'horizontal',
                    shadeIntensity: 0.25,
                    gradientToColors: undefined,
                    inverseColors: true,
                    opacityFrom: 0.85,
                    opacityTo: 0.85,
                    stops: [50, 0, 100],
                },
            },
        },
    }
    if (loading) {
        return <Loading />
    }
    return (
        <>
            <Box mb={2}>
                {data?.customerAmounts.customerAmounts
                    .filter((c) => activeApps?.includes(c.appName))
                    .map((c, i) => (
                        <Box display="inline-flex" p={2} key={i}>
                            <InfoCard
                                heading={labelMapping[c.amountType]}
                                children={formatPrice(c.amount)}
                            />
                        </Box>
                    ))}
                {data?.customerNumbers.customerNumbers
                    .filter((c) => activeApps?.includes(c.appName))
                    .map((c, i) => (
                        <Box display="inline-flex" p={2} key={i}>
                            <InfoCard
                                heading={labelMapping[c.numberType]}
                                children={formatNumber(c.number)}
                            />
                        </Box>
                    ))}
            </Box>

            <Box px={2}>
                <Typography>
                    <Trans>Avg revenue</Trans>
                </Typography>
            </Box>
            <ReactApexChart
                options={chartData.options}
                series={chartData.series}
                type="bar"
                height={350}
            />
            <Box py={8} />
        </>
    )
}

export default createFragmentContainer(CustomerStats, {
    customer: graphql`
        fragment CustomerStats_customer on Customer {
            id
        }
    `,
})
