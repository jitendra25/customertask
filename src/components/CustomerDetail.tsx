import { Trans } from '@lingui/macro'
import { Tab, Tabs, Theme } from '@material-ui/core'
import CardHeader from '@material-ui/core/CardHeader'
import { CloseOutlined, DeleteOutlined, EditOutlined, MailOutlined } from '@material-ui/icons'
import { makeStyles } from '@material-ui/styles'
import { ActionItem, Loading } from '@saastack/components'
import { useAlert, useConfig } from '@saastack/core'
import { Roles, useCan } from '@saastack/core/roles'
import hooks, { TabHook } from '@saastack/hooks'
import { DetailContainerProps } from '@saastack/layouts'
import { DetailContainer } from '@saastack/layouts/containers'
import { useQuery } from '@saastack/relay'
import { useNavigate, useParams } from '@saastack/router'
import { isMobileView, useDidMountEffect } from '@saastack/utils'
import clsx from 'clsx'
import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { CustomerDetail_customers } from '../__generated__/CustomerDetail_customers.graphql'
import { CustomerDetailQuery } from '../__generated__/CustomerDetailQuery.graphql'
import CustomerAvatar from './CustomerAvatar'
import CustomerInfo from './CustomerInfo'
import CustomerStats from './CustomerStats'
import CustomerPasswordResetMutation from '../mutations/CustomerPasswordResetMutation'
import { useRelayEnvironment } from 'react-relay/hooks'

const query = graphql`
    query CustomerDetailQuery($customerId: String) {
        customerLocationLinks(customerId: $customerId) {
            locationIds
        }
    }
`

export interface Props extends DetailContainerProps {
    customers: CustomerDetail_customers
    companyAdmin: boolean
    locationAdmin: boolean
}

const useStyles = makeStyles(({ palette: { divider } }: Theme) => ({
    cardHeader: {
        borderBottom: `1px solid ${divider}`,
    },
    tabs: {
        // backgroundColor: background.default
    },
    tab: {
        padding: 16,
        textTransform: 'uppercase',
        fontSize: '0.9rem!important',
        // '&.Mui-selected': {
        //     backgroundColor: background.paper
        // }
    },
}))

const CustomerDetail: React.FC<Props> = (componentProps) => {
    const showAlert = useAlert()
    const environment = useRelayEnvironment()
    const { customers, companyAdmin, locationAdmin, ...props } = componentProps
    const navigate = useNavigate()
    const { activeApps, companyId, locations, locationId } = useConfig()
    const can = useCan()
    const canManage = can([Roles.CustomersEditor, Roles.CustomersAdmin], companyId!)
    const [parent, setParent] = React.useState<string>(
        locations?.length! === 1 ? locationId! : companyId!
    )
    const [open, setOpen] = React.useState(true)
    const mobileView = isMobileView()
    const classes = useStyles()
    const mailRef = React.useRef<boolean | null>(false)
    const { id } = useParams()
    const customer = customers.find((i) => i?.id === window.atob(id!))

    const { data, loading, refetch } = useQuery<CustomerDetailQuery>(
        query,
        { customerId: customer?.id },
        { skip: !customer?.id || locations?.length! <= 1 }
    )
    const customerLocations = locations?.filter((loc) =>
        data?.customerLocationLinks?.locationIds?.includes(loc.node?.id!)
    )

    useDidMountEffect(() => {
        if (customer?.id && locations?.length! > 1) {
            refetch()
        }
    }, [customer?.id])
    React.useEffect(() => {
        if (data?.customerLocationLinks?.locationIds) {
            const locationId = data.customerLocationLinks.locationIds[0]
            setParent((canManage ? companyId : locationId)!)
        }
    }, [data])

    const navigateBack = () => {
        navigate('../')
        componentProps?.onClose?.()
    }
    const handleClose = () => setOpen(false)
    React.useEffect(() => {
        if (!customer) {
            navigateBack()
        }
    }, [customer])

    if (!customer) {
        return null
    }

    const tabs: TabHook[] = [
        { label: <Trans>Info</Trans>, component: CustomerInfo as any },
        ...(can([Roles.CustomersDataStatsViewer], parent)
            ? [
                  {
                      label: <Trans>Stats</Trans>,
                      component: CustomerStats as any,
                  },
              ]
            : []),
        ...hooks.customer.tab.getAllHooks(activeApps),
    ]

    const canUpdate = [...(data?.customerLocationLinks?.locationIds ?? []), companyId!].some((id) =>
        can([Roles.CustomersEditor, Roles.CustomersAdmin], id)
    )

    const handleResetPassword = () => {
        if (!mailRef.current) {
            mailRef.current = true
            CustomerPasswordResetMutation.commit(environment, customer.id, {
                onSuccess: () => {
                    showAlert(<Trans>Mail sent!</Trans>, {
                        variant: 'info',
                    })
                    mailRef.current = false
                },
                onError: (e) => {
                    showAlert(e, {
                        variant: 'error',
                    })
                    mailRef.current = false
                },
            })
        }
    }

    const actions: ActionItem[] = [
        ...(canUpdate
            ? [
                  {
                      title: <Trans>Update</Trans>,
                      icon: EditOutlined,
                      onClick: () => navigate('update'),
                  },
              ]
            : []),
        ...(canManage
            ? [
                  {
                      title: <Trans>Delete</Trans>,
                      icon: DeleteOutlined,
                      onClick: () => navigate('delete'),
                  },
              ]
            : []),
        ...(canUpdate
            ? [
                  {
                      title: <Trans>Send password reset mail</Trans>,
                      icon: MailOutlined,
                      onClick: handleResetPassword,
                  },
              ]
            : []),
        { title: <Trans>Close</Trans>, icon: CloseOutlined, onClick: handleClose },
    ]

    if (loading) {
        return (
            <DetailContainer open={open} onClose={handleClose} onExited={navigateBack} {...props}>
                <Loading />
            </DetailContainer>
        )
    }

    const header = (
        <>
            <CardHeader
                className={clsx(customerLocations?.length! > 1 && classes.cardHeader)}
                titleTypographyProps={{ variant: 'h5' }}
                title={
                    <>
                        {customer?.firstName} {customer?.lastName}
                    </>
                }
                subheader={customer?.email}
                avatar={<CustomerAvatar companyAdmin={companyAdmin} customer={customer!} />}
            />
            {customerLocations?.length! > 1 && (
                <Tabs
                    scrollButtons="auto"
                    className={classes.tabs}
                    value={parent}
                    onChange={(e, v) => setParent(v)}
                >
                    {canManage && (
                        <Tab
                            className={classes.tab}
                            value={companyId}
                            label={<Trans>Overall</Trans>}
                        />
                    )}
                    {customerLocations?.map((location, i) => (
                        <Tab
                            className={classes.tab}
                            value={location.node?.id}
                            key={i}
                            label={location.node?.name}
                        />
                    ))}
                </Tabs>
            )}
        </>
    )
    return (
        <DetailContainer
            actions={actions}
            open={open}
            onClose={handleClose}
            boxed
            onExited={navigateBack}
            header={header}
            tabProps={{
                parent,
                customerId: customer?.id,
                consumerId: customer?.id,
                customers,
                customer,
                locations: customerLocations,
                companyAdmin,
            }}
            tabs={tabs}
            {...props}
        />
    )
}

export default createFragmentContainer(CustomerDetail, {
    customers: graphql`
        fragment CustomerDetail_customers on Customer @relay(plural: true) {
            id
            firstName
            lastName
            email
            profileImage {
                largeImage
                thumbImage
            }
            ...CustomerInfo_customer
            ...CustomerStats_customer
            ...CustomerAvatar_customer
        }
    `,
})
