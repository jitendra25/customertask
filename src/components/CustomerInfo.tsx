import { t, Trans } from '@lingui/macro'
import { Box, DialogContent, Grid, List, ListItem, ListItemText, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { DateTime, Date } from '@saastack/components/i18n'
import { Alias, Location, useAlert, useAlias, useConfig } from '@saastack/core'
import { Roles, useCan } from '@saastack/core/roles'
import { ChipInput, LanguageDropdown, TimezoneDropdown } from '@saastack/forms'
import hooks, { InfoHook } from '@saastack/hooks'
import NoteAdd from '@saastack/notes/components/NoteAdd'
import { useQuery } from '@saastack/relay'
import { formatAddress, isMobileView, useDidMountEffect } from '@saastack/utils'
import clsx from 'clsx'
import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { useRelayEnvironment } from 'react-relay/hooks'
import { CustomerInfo_customer } from '../__generated__/CustomerInfo_customer.graphql'
import { CustomerInfoQuery } from '../__generated__/CustomerInfoQuery.graphql'
import { CustomerInput } from '../__generated__/UpdateCustomerMutation.graphql'
import UpdateCustomerMutation from '../mutations/UpdateCustomerMutation'
import { PubSub } from '@saastack/pubsub'
import namespace from '@saastack/notes/namespace'
import LocationWidget from '@saastack/locations/components/LocationWidget'
import BoxWidget from '../widgets/BoxWidget'

const useStyles = makeStyles(({ spacing, palette: { background } }: Theme) => ({
    header: {
        '& .box-widget-header': {
            padding: spacing(0, 0, 0, 3),
        },
    },
    list: {
        padding: spacing(1, 1),
    },
    listItemText: {
        paddingTop: 0,
        paddingBottom: 0,
        '&:nth-child(2)': {
            '& .MuiListItemText-root:first-child': {
                alignSelf: 'flex-start',
            },
        },
        '& .MuiListItemText-root:first-child': {
            flexShrink: 0,
            marginRight: 8,
        },
        '& .MuiListItemText-root:last-child': {
            textAlign: 'right',
            alignSelf: 'flex-start',

            '& .MuiSelect-root.MuiSelect-select': {
                marginLeft: -8,
            },
        },
    },
    listItemText2: {
        '& .MuiListItemText-root:first-child': {
            flex: `0 0 100px`,
        },
        '& .MuiListItemText-root:last-child': {
            textAlign: 'left',
        },
    },
    note: {
        '& .MuiButtonBase-root.MuiIconButton-root.MuiExpansionPanelSummary-expandIcon': {
            display: 'none',
        },
        '&.MuiExpansionPanel-root.Mui-disabled': {
            backgroundColor: 'transparent',
        },
        '& .MuiExpansionPanelSummary-root.Mui-disabled': {
            opacity: 1,
        },
    },
    withInput: {
        '& .MuiInputBase-root.MuiFilledInput-root': {
            '&:hover, &.Mui-focused, &:focus': {
                backgroundColor: 'transparent',
            },
        },
    },
}))

interface Props {
    companyAdmin: boolean
    locationAdmin: boolean
    locations?: Location[]
    parent: string
    customer: CustomerInfo_customer
}

const query = graphql`
    query CustomerInfoQuery(
        $parent: String
        $customerId: String
        $note: Boolean!
        $activity: Boolean!
    ) {
        customerActivities(customerId: $customerId, parent: $parent) @include(if: $activity) {
            customerActivities {
                activityType
                date
                appName
            }
        }
        notes(
            first: 1
            parent: $parent
            against: $customerId
            statusType: PRIVATE
            getPrivate: true
            allStaff: false
            appTypeName: ["Customers"]
        ) @include(if: $note) {
            edges {
                node {
                    id
                    description
                    typeId
                    private
                    appTypeName
                }
            }
        }
    }
`

const labelMapping = (alias: Alias): Record<string, React.ReactNode> => ({
    ADDED_ON: <Trans>{alias.singular} Since</Trans>,
    LAST_ACTIVITY_ON: <Trans>Last active</Trans>,
    LAST_APPOINTMENT_BOOKED_ON: <Trans>Last appointment booked</Trans>,
    LAST_BOOKED_ON: <Trans>Last booked</Trans>,
    LAST_CLASS_BOOKED_ON: <Trans>Last class booked</Trans>,
    LAST_PURCHASED_ON: <Trans>Last purchased</Trans>,
    LAST_REVIEWED_ON: <Trans>Last reviewed</Trans>,
})

const CustomerInfo: React.FC<Props> = ({
    customer,
    companyAdmin,
    locationAdmin,
    parent,
    locations,
}) => {
    const alias = useAlias('Customers', { singular: 'Customer', plural: 'Customers' })
    const locationAlias = useAlias('Locations', {
        singular: 'Location',
        plural: 'Locations',
    })
    const can = useCan()
    const { activeApps, locations: allLocations, timezone } = useConfig()
    const [customerLocations, setCustomerLocations] = React.useState<string[]>([])
    const activityAccess = can([Roles.CustomersDataActivityViewer], parent)
    const noteAccess = can([Roles.NotesPrivateAccess], parent)
    const variables: CustomerInfoQuery['variables'] = {
        customerId: customer?.id,
        note: noteAccess,
        activity: activityAccess,
        parent,
    }
    const { data, refetch } = useQuery<CustomerInfoQuery>(query, variables)
    const mobileView = isMobileView('xs')

    useDidMountEffect(() => {
        refetch()
    }, [customer?.id, parent])
    const classes = useStyles()

    const environment = useRelayEnvironment()
    const showAlert = useAlert()

    React.useEffect(() => {
        const id = PubSub.subscribe(namespace?.delete, () => {
            refetch()
        })
        return () => PubSub.unsubscribe(id)
    }, [])

    const handleChange = (e: React.ChangeEvent<any>) => {
        const { name, value } = e.target
        const updatedCustomer: CustomerInput = {
            id: customer?.id,
            [name]: value,
        }
        UpdateCustomerMutation.commit(
            environment,
            updatedCustomer,
            [name],
            Boolean(companyAdmin || locationAdmin),
            {
                onError,
                onSuccess,
            }
        )
    }

    const handleLocationChange = (loc: string[]) => {
        setCustomerLocations(loc)
        const updatedCustomer: CustomerInput = {
            id: customer?.id,
        }
        UpdateCustomerMutation.commit(
            environment,
            updatedCustomer,
            ['preferredLanguage'],
            Boolean(companyAdmin || locationAdmin),
            {
                onError,
                onSuccess,
            },
            loc
        )
    }
    const onError = (e: string) => {
        showAlert(e, {
            variant: 'error',
        })
    }

    const onSuccess = () => {
        showAlert(<Trans>{alias?.singular} updated successfully!</Trans>, {
            variant: 'info',
        })
    }
    const hookItems: InfoHook[] = hooks.customer.info.getAllHooks(activeApps)
    const infoItems = [
        { label: <Trans>Company</Trans>, value: customer?.companyId },
        {
            label: (
                <Box mt={0.5}>
                    <Trans>Address</Trans>
                </Box>
            ),
            value: formatAddress(customer?.address),
        },
        { label: <Trans>Telephones</Trans>, value: customer?.telephones?.join(', ') },
        {
            label: <Trans>Language</Trans>,
            value: (
                <Box ml={0.65}>
                    <LanguageDropdown
                        placeholder={t`Select language`}
                        onChange={handleChange}
                        name="preferredLanguage"
                        value={customer?.preferredLanguage}
                    />
                </Box>
            ),
        },
        {
            label: <Trans>Timezone</Trans>,
            value: (
                <Box>
                    <TimezoneDropdown
                        placeholder={t`Select timezone`}
                        onChange={handleChange as any}
                        name="timezone"
                        value={(customer?.timezone as any) || ' '}
                    />
                </Box>
            ),
        },
    ]
    const specialDates = [
        { label: <Trans>Birth Date</Trans>, value: <Date time={customer.birthDate} /> },
    ]
    const lastActivityItems =
        data?.customerActivities?.customerActivities
            .filter((ac) => activeApps?.includes(ac.appName))
            .map((ac) => ({
                label: labelMapping(alias!)[ac.activityType],
                value: <DateTime relative={87600} time={ac.date as string} />,
            })) ?? []
    const note = data?.notes?.edges?.[0]?.node ?? {
        id: '',
        description: '',
        typeId: customer.id,
        appTypeName: 'Customers',
    }
    const handleNoteUpdate = (callback: () => void) => {
        refetch()
        callback()
    }
    return (
        <Box pb={8}>
            <Grid container wrap="wrap-reverse" spacing={4}>
                <Grid item xs={12} sm={6}>
                    <BoxWidget header={<Trans>Details</Trans>} card>
                        <List className={classes.list}>
                            {infoItems.map((c, i) => (
                                <ListItem
                                    className={clsx(classes.listItemText2, classes.listItemText)}
                                    key={i}
                                >
                                    <ListItemText secondary={c.label} />
                                    <ListItemText primary={c.value} />
                                </ListItem>
                            ))}
                        </List>
                    </BoxWidget>
                    <BoxWidget header={<Trans>Special Dates</Trans>} card>
                        <List className={classes.list}>
                            {specialDates.map((c, i) => (
                                <ListItem
                                    className={clsx(classes.listItemText2, classes.listItemText)}
                                    key={i}
                                >
                                    <ListItemText secondary={c.label} />
                                    <ListItemText primary={c.value} />
                                </ListItem>
                            ))}
                        </List>
                    </BoxWidget>
                    {/* {allLocations?.length! > 1 && locations?.length! > 0 && (
                        <BoxWidget header={<Trans>{locationAlias?.plural}</Trans>} card>
                            { (<List className={classes.list}>
                                {locations!.map((location, i) => (
                                    <ListItem key={i}>
                                        <ListItemText primary={location.node?.name} />
                                    </ListItem>
                                ))}
                            </List>)}
                        </BoxWidget>
                    )} */}
                    {allLocations?.length! > 1 && (
                        <BoxWidget header={<Trans>{locationAlias?.plural}</Trans>} card>
                            {locations?.length! > 0 ? (
                                <List className={classes.list}>
                                    {locations!.map((location, i) => (
                                        <ListItem key={i}>
                                            <ListItemText primary={location.node?.name} />
                                        </ListItem>
                                    ))}
                                </List>
                            ) : (
                                <LocationWidget
                                    multiple={true}
                                    name="locationIds"
                                    searchProps={{
                                        placeholder: t`Select Location `,
                                        variant: 'inline' as any,
                                    }}
                                    value={customerLocations}
                                    onChange={(e) => {
                                        handleLocationChange(e.target.value)
                                    }}
                                    grid={{ xs: 11 }}
                                    // className={classes.location}
                                />
                            )}
                        </BoxWidget>
                    )}

                    {/* {allLocations?.length! > 1 && (
                        <BoxWidget header={<Trans>{locationAlias?.plural}</Trans>} card>
                            {locations?.length! > 0 ? (<List className={classes.list}>
                                {locations!.map((location, i) => (
                                    <ListItem key={i}>
                                        <ListItemText primary={location.node?.name} />
                                    </ListItem>
                                ))}
                            </List>) : <LocationWidget
                                    multiple={true}
                                    name="locationIds"
                                    searchProps={{
                                        placeholder: t`Select Location `,
                                        variant: 'inline' as any,
                                    }}
                                    onChange={(e) => { handleLocationChange(e.target.value) }}
                                    grid={{ xs: 11 }}
                                // className={classes.location}
                                />}
                        </BoxWidget>
                    )} */}

                    {hookItems.map((item, i) => {
                        const C = item.component as any
                        return (
                            <C
                                key={i}
                                parent={customer?.id}
                                customer={customer}
                                locationId={parent}
                                locations={locations}
                            />
                        )
                    })}
                </Grid>
                <Grid item xs={12} sm={6}>
                    {noteAccess && (
                        <BoxWidget
                            className={clsx(classes.note, classes.withInput, classes.header)}
                            header={<Trans>Private note</Trans>}
                        >
                            <Box px={2} pb={1}>
                                <NoteAdd
                                    rows={4}
                                    rowsMax={10}
                                    margin="none"
                                    onChange={handleNoteUpdate}
                                    note={note}
                                    update={Boolean(note.id)}
                                    customerId={customer?.id!}
                                    parent={parent}
                                />
                            </Box>
                        </BoxWidget>
                    )}
                    {activityAccess && (
                        <BoxWidget className={classes.header} header={<Trans>Activity</Trans>}>
                            <List className={clsx(classes.list)}>
                                {lastActivityItems?.map((c, i) => (
                                    <ListItem className={classes.listItemText} key={i}>
                                        <ListItemText secondary={c.label} />
                                        <ListItemText primary={c.value} />
                                    </ListItem>
                                ))}
                            </List>
                        </BoxWidget>
                    )}
                    <BoxWidget
                        className={clsx(classes.withInput, classes.header)}
                        header={<Trans>Tags</Trans>}
                    >
                        <Box px={2}>
                            <ChipInput
                                alwaysShowPlaceholder
                                placeholder={t`+ Add tag` as any}
                                margin="none"
                                onChange={handleChange as any}
                                name="tag"
                                variant={'inline' as any}
                                value={customer?.tag as any}
                                newChipKeys={['Enter', ' ', ',']}
                            />
                        </Box>
                    </BoxWidget>
                </Grid>
            </Grid>
        </Box>
    )
}

export default createFragmentContainer(CustomerInfo, {
    customer: graphql`
        fragment CustomerInfo_customer on Customer {
            id
            companyId
            address {
                country
                locality
                postalCode
                region
                streetAddress
            }
            birthDate
            telephones
            timezone
            tag
            preferredLanguage
        }
    `,
})
