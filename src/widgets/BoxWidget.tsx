import React from 'react'
import { makeStyles } from '@material-ui/styles'
import { Box, Card, Theme, Typography } from '@material-ui/core'
import clsx from 'clsx'

interface Props {
    header: React.ReactNode
    className?: string
    border?: boolean
    card?: boolean
}

const useStyles = makeStyles(
    ({
        palette: { text, divider },
        typography: { body2 },
        spacing,
        shape: { borderRadius },
        breakpoints: { down },
    }: Theme) => ({
        header: {
            ...body2,
            textTransform: 'uppercase',
            color: text.secondary,
            margin: spacing(1, 0),
        },
        border: {
            border: `1px solid ${divider}`,
            borderRadius,
            padding: spacing(1, 0),
        },
        box: {
            margin: spacing(4, 0),
            [down('xs')]: {
                margin: spacing(2, 0),
            },
        },
    })
)

const BoxWidget: React.FC<Props> = ({ header, children, className, border, card }) => {
    const classes = useStyles()

    const child = (
        <Box className={clsx('box-widget-children', { [classes.border]: border })}>{children}</Box>
    )

    return (
        <Box className={clsx(classes.box, className)}>
            <Box className={classes.header}>
                <Typography className="box-widget-header">{header}</Typography>
            </Box>
            {card ? <Card>{child}</Card> : child}
        </Box>
    )
}

export default BoxWidget
