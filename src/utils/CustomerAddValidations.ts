import { t } from '@lingui/macro'
import * as yup from 'yup'
import 'yup-phone'

const CustomerAddValidations = yup.object().shape({
    name: yup
        .string()
        .max(50, (t`Max 50 characters allowed` as unknown) as string)
        .required((t`Required` as unknown) as string),
    email: yup
        .string()
        .required((t`Required` as unknown) as string)
        .email((t`Invalid email` as unknown) as string),
})

export default CustomerAddValidations
