import { t } from '@lingui/macro'
import * as yup from 'yup'

const phoneRegExp = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/
const BulkImportCustomerAddValidations = yup.object().shape({
    firstName: yup.string().required((`first name is Required` as unknown) as string),
    email: yup
        .string()
        .required((`email is Required` as unknown) as string)
        .email((`Invalid email` as unknown) as string),
    lastname: yup.string(),
    source: yup.string(),
    medium: yup.string(),
    campaign: yup.string(),
    profileImage: yup.object().shape({
        largeImage: yup.string(),
    }),
    preferredLanguage: yup.string(),
    address: yup.object().shape({
        locality: yup.string(),
        state: yup.string(),
        postalCode: yup.string(),
        country: yup.string(),
    }),
})

export default BulkImportCustomerAddValidations
