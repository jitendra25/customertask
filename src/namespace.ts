const namespace = {
    fetch: 'customer/fetch',
    create: 'customer/create',
    update: 'customer/update',
    delete: 'customer/delete',
}

export default namespace
