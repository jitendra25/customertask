# Changelog

All notable changes to this project will be documented in this file.
See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.191](https://gitlab.com/saastack/ui/customers/compare/v0.1.190...v0.1.191) (2021-03-22)

### Bug Fixes

-   empty location
    ids ([32d4f27](https://gitlab.com/saastack/ui/customers/commit/32d4f278de9a4579d20a891d550965d6b61b65fe))

### [0.1.190](https://gitlab.com/saastack/ui/customers/compare/v0.1.189...v0.1.190) (2021-03-18)

### Bug Fixes

-   password reset mail error
    message ([c40b26e](https://gitlab.com/saastack/ui/customers/commit/c40b26e2fd9545cf32a3f7d814efec00b79e7f27))

### [0.1.189](https://gitlab.com/saastack/ui/customers/compare/v0.1.188...v0.1.189) (2021-03-12)

### Features

-   password reset
    mail ([687259e](https://gitlab.com/saastack/ui/customers/commit/687259e687793ac4de89b8e4000d8eb96ba4a0c3))

### [0.1.188](https://gitlab.com/saastack/ui/customers/compare/v0.1.187...v0.1.188) (2021-03-12)

### Features

-   password reset
    mail ([6bddd7b](https://gitlab.com/saastack/ui/customers/commit/6bddd7bb031441029ce3a335e8789f429e26d5bb))

### [0.1.187](https://gitlab.com/saastack/ui/customers/compare/v0.1.186...v0.1.187) (2021-03-11)

### Bug Fixes

-   customer end date in registration date
    filter ([85f0170](https://gitlab.com/saastack/ui/customers/commit/85f01704e0aa12e0606d51a1d48b99133768a96a))

### [0.1.186](https://gitlab.com/saastack/ui/customers/compare/v0.1.185...v0.1.186) (2021-03-10)

### Bug Fixes

-   customer location links
    issue ([0def676](https://gitlab.com/saastack/ui/customers/commit/0def676b396a24bb5e6367cd13c7a4161200559c))

### [0.1.185](https://gitlab.com/saastack/ui/customers/compare/v0.1.184...v0.1.185) (2021-03-03)

### Bug Fixes

-   customer report
    query ([d99827e](https://gitlab.com/saastack/ui/customers/commit/d99827e9aeb2cd433561f43d86b08bcf3e26a637))

### [0.1.184](https://gitlab.com/saastack/ui/customers/compare/v0.1.183...v0.1.184) (2021-02-26)

### Features

-   upload ([5f7b44b](https://gitlab.com/saastack/ui/customers/commit/5f7b44b6eeeb479a551d7160e5ef4e0c675c4928))

### [0.1.183](https://gitlab.com/saastack/ui/customers/compare/v0.1.182...v0.1.183) (2021-02-23)

### Features

-   report download ([b8fcde4](https://gitlab.com/saastack/ui/customers/commit/b8fcde490d7828449caace0b1abfec77565d7cc5))

### [0.1.182](https://gitlab.com/saastack/ui/customers/compare/v0.1.181...v0.1.182) (2021-02-18)

### Features

-   sorting ([f53cd62](https://gitlab.com/saastack/ui/customers/commit/f53cd620693b683b29b90ffca50b82598cd606a3))

### [0.1.181](https://gitlab.com/saastack/ui/customers/compare/v0.1.180...v0.1.181) (2021-02-13)

### Features

-   birthdate ([8323e0e](https://gitlab.com/saastack/ui/customers/commit/8323e0eda4df414d9f84ebd6d755dc58f19a212d))

### Bug Fixes

-   customer
    timezone ([afc1d6c](https://gitlab.com/saastack/ui/customers/commit/afc1d6ce58f6683e415b33d16682d9cda9745a3b))

### [0.1.180](https://gitlab.com/saastack/ui/customers/compare/v0.1.179...v0.1.180) (2021-02-13)

### Bug Fixes

-   new chip keys ([3a19a74](https://gitlab.com/saastack/ui/customers/commit/3a19a74e94522c88815aa1162a0b981ae74e432f))
-   timezone
    dropdown ([8828a32](https://gitlab.com/saastack/ui/customers/commit/8828a32c531d82602760464cab9ede84514b3d64))

### [0.1.179](https://gitlab.com/saastack/ui/customers/compare/v0.1.178...v0.1.179) (2021-02-12)

### Bug Fixes

-   height in
    details ([c9f1d48](https://gitlab.com/saastack/ui/customers/commit/c9f1d488face92cf1f98ff66f7b026209d35196f))

### [0.1.178](https://gitlab.com/saastack/ui/customers/compare/v0.1.177...v0.1.178) (2021-02-03)

### Bug Fixes

-   locations in customer add
    form ([030bcd8](https://gitlab.com/saastack/ui/customers/commit/030bcd8f33bebcdc7b1a71b8c85d8cd665700e43))

### [0.1.177](https://gitlab.com/saastack/ui/customers/compare/v0.1.176...v0.1.177) (2021-02-01)

### Bug Fixes

-   customer detail
    changes ([c550dd6](https://gitlab.com/saastack/ui/customers/commit/c550dd617f278123319354763369bdd222214b48))

### [0.1.176](https://gitlab.com/saastack/ui/customers/compare/v0.1.175...v0.1.176) (2021-01-22)

### [0.1.175](https://gitlab.com/saastack/ui/customers/compare/v0.1.174...v0.1.175) (2021-01-22)

### Features

-   hover checkbox ([8c231d0](https://gitlab.com/saastack/ui/customers/commit/8c231d0004bf82a8919e8f956933cfb02bf1d879))

### [0.1.174](https://gitlab.com/saastack/ui/customers/compare/v0.1.173...v0.1.174) (2021-01-19)

### Features

-   aliasing in
    reports ([285ddb3](https://gitlab.com/saastack/ui/customers/commit/285ddb3e1ee1ec4644b733dd393b611566c62537))

### [0.1.173](https://gitlab.com/saastack/ui/customers/compare/v0.1.172...v0.1.173) (2021-01-19)

### Bug Fixes

-   assign locations to
    customer ([7c6ff65](https://gitlab.com/saastack/ui/customers/commit/7c6ff65a71a7583d16b18cd7e83d289f68521965))

### [0.1.172](https://gitlab.com/saastack/ui/customers/compare/v0.1.169...v0.1.172) (2021-01-19)

### Bug Fixes

-   hide import ([a7ca3fc](https://gitlab.com/saastack/ui/customers/commit/a7ca3fc0d0191b10dc35275f031fb44741e515b1))
-   priority points ([0597427](https://gitlab.com/saastack/ui/customers/commit/0597427215aca341726ad2782ee87b38aea73165))

### [0.1.169](https://gitlab.com/saastack/ui/customers/compare/v0.1.168...v0.1.169) (2021-01-19)

### Features

-   aliasing in
    reports ([febec44](https://gitlab.com/saastack/ui/customers/commit/febec449684a211feaf78b60bf090c0115866e49))

### [0.1.168](https://gitlab.com/saastack/ui/customers/compare/v0.1.167...v0.1.168) (2021-01-13)

### Bug Fixes

-   build issue ([0b62b4a](https://gitlab.com/saastack/ui/customers/commit/0b62b4ad2811f1ee8c86ed4061a0683cf9a0e7f6))
-   report query ([f4efc43](https://gitlab.com/saastack/ui/customers/commit/f4efc43c8ae8fff99fc3fb1301663cc97bd7a9b3))
-   report roles and detailed
    report ([ea71fd5](https://gitlab.com/saastack/ui/customers/commit/ea71fd5aea7cf643926b0214f91a665d946b8854))

### [0.1.167](https://gitlab.com/saastack/ui/customers/compare/v0.1.166...v0.1.167) (2021-01-07)

### [0.1.166](https://gitlab.com/saastack/ui/customers/compare/v0.1.165...v0.1.166) (2021-01-06)

### [0.1.165](https://gitlab.com/saastack/ui/customers/compare/v0.1.164...v0.1.165) (2021-01-05)

### Bug Fixes

-   customer clickable in
    report ([64c6178](https://gitlab.com/saastack/ui/customers/commit/64c61787f968df0a6b1f87dca7869575cbb86a85))

### [0.1.164](https://gitlab.com/saastack/ui/customers/compare/v0.1.163...v0.1.164) (2021-01-04)

### Bug Fixes

-   parent company
    id ([cfb0901](https://gitlab.com/saastack/ui/customers/commit/cfb0901e9cee1908e13326a11d322fe0c02083e4))

### [0.1.163](https://gitlab.com/saastack/ui/customers/compare/v0.1.162...v0.1.163) (2021-01-04)

### Bug Fixes

-   build issues ([9300b12](https://gitlab.com/saastack/ui/customers/commit/9300b12495baed576167bc8ca0a43af215121075))

### [0.1.162](https://gitlab.com/saastack/ui/customers/compare/v0.1.161...v0.1.162) (2021-01-04)

### Bug Fixes

-   location admin ([aca8f12](https://gitlab.com/saastack/ui/customers/commit/aca8f1259938a85e52ac922571410eccc5027bb4))

### [0.1.161](https://gitlab.com/saastack/ui/customers/compare/v0.1.160...v0.1.161) (2021-01-02)

### Bug Fixes

-   aliasing ([ec818e4](https://gitlab.com/saastack/ui/customers/commit/ec818e44fb3dd0bb4160b17560498bee551db2fb))

### [0.1.160](https://gitlab.com/saastack/ui/customers/compare/v0.1.159...v0.1.160) (2021-01-02)

### Features

-   get customer in customer detail
    page ([819a4a1](https://gitlab.com/saastack/ui/customers/commit/819a4a1a27906b3b2abf0ecf2496dd42ed34d333))

### [0.1.159](https://gitlab.com/saastack/ui/customers/compare/v0.1.158...v0.1.159) (2020-12-30)

### Features

-   no result
    component ([cfe3b92](https://gitlab.com/saastack/ui/customers/commit/cfe3b9298e975b5b2d60c989d22e812522ae6310))

### [0.1.158](https://gitlab.com/saastack/ui/customers/compare/v0.1.157...v0.1.158) (2020-12-30)

### Features

-   aliasing ([af20d96](https://gitlab.com/saastack/ui/customers/commit/af20d964baf0aefafaee594890076e451cd06b41))

### [0.1.157](https://gitlab.com/saastack/ui/customers/compare/v0.1.156...v0.1.157) (2020-12-24)

### [0.1.156](https://gitlab.com/saastack/ui/customers/compare/v0.1.155...v0.1.156) (2020-12-22)

### [0.1.155](https://gitlab.com/saastack/ui/customers/compare/v0.1.154...v0.1.155) (2020-12-18)

### [0.1.154](https://gitlab.com/saastack/ui/customers/compare/v0.1.153...v0.1.154) (2020-12-16)

### Features

-   location widget in add form
    component ([b951686](https://gitlab.com/saastack/ui/customers/commit/b951686a64247678e9230b34c3c9949ef71676c1))

### Bug Fixes

-   new versions ([1446b1f](https://gitlab.com/saastack/ui/customers/commit/1446b1fa3af30d8ba623d518cc56127ce476a5c8))
-   new versions ([2c56be3](https://gitlab.com/saastack/ui/customers/commit/2c56be36786fc6bc61de0dc9f1574ecaab1ee070))

### [0.1.153](https://gitlab.com/saastack/ui/customers/compare/v0.1.152...v0.1.153) (2020-12-16)

### [0.1.152](https://gitlab.com/saastack/ui/customers/compare/v0.1.151...v0.1.152) (2020-12-16)

### Features

-   added customer search
    initial ([5521d52](https://gitlab.com/saastack/ui/customers/commit/5521d52a6a677367bad2e79ab46d49e6074c28a6))
-   added selected value
    changes ([9aa1679](https://gitlab.com/saastack/ui/customers/commit/9aa1679dcb42cefca3efb4f447033d16613d86e7))
-   fixed issues and added input para to customer
    add ([7a76ec8](https://gitlab.com/saastack/ui/customers/commit/7a76ec893f9435c6faeed58bb1219bc7140e1d03))
-   fixes issues and todo
    chanages ([243d815](https://gitlab.com/saastack/ui/customers/commit/243d81560c583e85cb9c0a1c69b0e27bc4d1caa0))
-   removed unsed
    variables ([659cf2d](https://gitlab.com/saastack/ui/customers/commit/659cf2d02f5dbcae69bf8dc13e4d0a518ab9367d))

### Bug Fixes

-   add issue ([b9ed577](https://gitlab.com/saastack/ui/customers/commit/b9ed577f8e63564409e2fc1f7c6cf0337c81c8ac))
-   updater in create
    mutation ([a93d400](https://gitlab.com/saastack/ui/customers/commit/a93d400351b90c4ae9b6b35f318021555c9497d1))

### [0.1.151](https://gitlab.com/saastack/ui/customers/compare/v0.1.150...v0.1.151) (2020-12-09)

### [0.1.150](https://gitlab.com/saastack/ui/customers/compare/v0.1.149...v0.1.150) (2020-12-04)

### Bug Fixes

-   consumer id ([81bee52](https://gitlab.com/saastack/ui/customers/commit/81bee5215cef8c57037871c63fd375fb31c800eb))

### [0.1.149](https://gitlab.com/saastack/ui/customers/compare/v0.1.148...v0.1.149) (2020-12-01)

### Features

-   **reports:** config param
    added ([50558b6](https://gitlab.com/saastack/ui/customers/commit/50558b6551403f3898821feefe0ea3db7a7a5a80))

### [0.1.148](https://gitlab.com/saastack/ui/customers/compare/v0.1.147...v0.1.148) (2020-11-21)

### Bug Fixes

-   filters ([e8ff4ae](https://gitlab.com/saastack/ui/customers/commit/e8ff4aedc43ab36d83f3054f22466b3dcaeb9228))

### [0.1.147](https://gitlab.com/saastack/ui/customers/compare/v0.1.146...v0.1.147) (2020-11-17)

### Bug Fixes

-   customer click ([e30a5e0](https://gitlab.com/saastack/ui/customers/commit/e30a5e0a43ac297454cf2c0936a86b1d55aabad8))

### [0.1.146](https://gitlab.com/saastack/ui/customers/compare/v0.1.145...v0.1.146) (2020-11-16)

### [0.1.145](https://gitlab.com/saastack/ui/customers/compare/v0.1.144...v0.1.145) (2020-11-12)

### Features

-   **index:** parameter name
    fix ([378b39b](https://gitlab.com/saastack/ui/customers/commit/378b39bf273b089c731f84d50f7b4f7be2709485))

### [0.1.144](https://gitlab.com/saastack/ui/customers/compare/v0.1.143...v0.1.144) (2020-11-12)

### Features

-   **notification:** onClose
    added ([fcac92d](https://gitlab.com/saastack/ui/customers/commit/fcac92ddd7db642b1fe55aa96f7538cb64297276))

### [0.1.143](https://gitlab.com/saastack/ui/customers/compare/v0.1.142...v0.1.143) (2020-11-12)

### Features

-   **form:** validations
    added ([d6373ab](https://gitlab.com/saastack/ui/customers/commit/d6373abca28ac664e413d29d2c3910ec41c9c351))

### [0.1.142](https://gitlab.com/saastack/ui/customers/compare/v0.1.141...v0.1.142) (2020-11-07)

### Features

-   action hooks ([b372adc](https://gitlab.com/saastack/ui/customers/commit/b372adcec921ee0d0be0491cbbe1dd8e8d0a9b27))

### [0.1.141](https://gitlab.com/saastack/ui/customers/compare/v0.1.140...v0.1.141) (2020-11-06)

### Bug Fixes

-   update customer from
    appointment ([73cf10d](https://gitlab.com/saastack/ui/customers/commit/73cf10dce90fe193c9ce825fadd564836f0252d1))

### [0.1.140](https://gitlab.com/saastack/ui/customers/compare/v0.1.139...v0.1.140) (2020-11-06)

### Bug Fixes

-   update customer from
    appointment ([73cf10d](https://gitlab.com/saastack/ui/customers/commit/73cf10dce90fe193c9ce825fadd564836f0252d1))

### [0.1.139](https://gitlab.com/saastack/ui/customers/compare/v0.1.138...v0.1.139) (2020-11-06)

### Features

-   info hooks
    changes ([9edd10e](https://gitlab.com/saastack/ui/customers/commit/9edd10e691b895feac573ac473d508733cb1e0c1))

### [0.1.138](https://gitlab.com/saastack/ui/customers/compare/v0.1.137...v0.1.138) (2020-11-06)

### Features

-   info hooks
    changes ([0d7a471](https://gitlab.com/saastack/ui/customers/commit/0d7a471e51f85fb0a3f4aa866dd3be026cd33180))

### [0.1.137](https://gitlab.com/saastack/ui/customers/compare/v0.1.136...v0.1.137) (2020-10-31)

### Features

-   actions ([f191779](https://gitlab.com/saastack/ui/customers/commit/f191779d4126caa0a6262812849fbe22ccbb47ca))

### [0.1.136](https://gitlab.com/saastack/ui/customers/compare/v0.1.135...v0.1.136) (2020-10-31)

### Features

-   info basic
    changes ([8d78673](https://gitlab.com/saastack/ui/customers/commit/8d7867351acf4729c04d8ecc2915bb5227e7203f))

### [0.1.135](https://gitlab.com/saastack/ui/customers/compare/v0.1.134...v0.1.135) (2020-10-30)

### Features

-   convert info to
    tab ([26367ea](https://gitlab.com/saastack/ui/customers/commit/26367ea176172a591172c1f6d10ae6f4db893fb9))
-   info basic
    changes ([d3fb786](https://gitlab.com/saastack/ui/customers/commit/d3fb786afd27472d10f6a6bd7899e7ccec519c19))

### [0.1.134](https://gitlab.com/saastack/ui/customers/compare/v0.1.133...v0.1.134) (2020-10-29)

### Features

-   **reports:** new ux
    fix ([bf0421b](https://gitlab.com/saastack/ui/customers/commit/bf0421bdbbaca41d7f10fe40fa974496dce52f4c))

### [0.1.133](https://gitlab.com/saastack/ui/customers/compare/v0.1.132...v0.1.133) (2020-10-28)

### Features

-   **reports:** new ux
    fix ([8dc8bda](https://gitlab.com/saastack/ui/customers/commit/8dc8bdac7325a32ab326cd7397f2cd173ad4eee6))

### [0.1.132](https://gitlab.com/saastack/ui/customers/compare/v0.1.131...v0.1.132) (2020-10-28)

### Features

-   **reports:** new ux
    fix ([b08b8d6](https://gitlab.com/saastack/ui/customers/commit/b08b8d6cfcccf3eb47dda7ab3012f4af4558aa82))

### [0.1.131](https://gitlab.com/saastack/ui/customers/compare/v0.1.130...v0.1.131) (2020-10-28)

### Features

-   **reports:** new
    ux ([942470c](https://gitlab.com/saastack/ui/customers/commit/942470ce00efb41c4edf0f6cf160b015c9a5a5f0))

### [0.1.130](https://gitlab.com/saastack/ui/customers/compare/v0.1.129...v0.1.130) (2020-10-23)

### Bug Fixes

-   added list
    virtualization ([aa568d9](https://gitlab.com/saastack/ui/customers/commit/aa568d9ddffcc1460273ab63a6c9de0b953ed15b))

### [0.1.129](https://gitlab.com/saastack/ui/customers/compare/v0.1.128...v0.1.129) (2020-10-23)

### Bug Fixes

-   added list
    virtualization ([1fb81b6](https://gitlab.com/saastack/ui/customers/commit/1fb81b68c535ab4aa4055a326ebadb4bd867c29a))

### [0.1.128](https://gitlab.com/saastack/ui/customers/compare/v0.1.127...v0.1.128) (2020-09-10)

### Bug Fixes

-   activity items ([72601f1](https://gitlab.com/saastack/ui/customers/commit/72601f1cef4e889739487bcf0a1150bf971f7ec8))

### [0.1.127](https://gitlab.com/saastack/ui/customers/compare/v0.1.126...v0.1.127) (2020-09-09)

### Bug Fixes

-   build issues ([29f3489](https://gitlab.com/saastack/ui/customers/commit/29f3489af7a3d5b0e9d2bf4412cca7c6f38c9180))

### [0.1.126](https://gitlab.com/saastack/ui/customers/compare/v0.1.125...v0.1.126) (2020-09-09)

### Bug Fixes

-   image remove ([1510192](https://gitlab.com/saastack/ui/customers/commit/1510192bdf0f27b7c5e7f92d42784dc30a028de9))

### [0.1.125](https://gitlab.com/saastack/ui/customers/compare/v0.1.124...v0.1.125) (2020-09-08)

### Features

-   notify via email ([b45a0ec](https://gitlab.com/saastack/ui/customers/commit/b45a0ece25af4439b143aae4e0a4bbe1efb9600a))

### Bug Fixes

-   disableFuture ([94f603f](https://gitlab.com/saastack/ui/customers/commit/94f603f4853469ca646c526ee4e4361ffd1f1467))

### [0.1.124](https://gitlab.com/saastack/ui/customers/compare/v0.1.123...v0.1.124) (2020-08-18)

### Bug Fixes

-   relay runtime ([ff4f0b6](https://gitlab.com/saastack/ui/customers/commit/ff4f0b60a2bf26ee3a218088e3e2613a6fcafa6e))

### [0.1.123](https://gitlab.com/saastack/ui/customers/compare/v0.1.122...v0.1.123) (2020-08-18)

### Bug Fixes

-   relay update ([fbbdd43](https://gitlab.com/saastack/ui/customers/commit/fbbdd430d38906a19ca7fe8b3014c25728bf447f))

### [0.1.122](https://gitlab.com/saastack/ui/customers/compare/v0.1.120...v0.1.122) (2020-08-18)

### Bug Fixes

-   relay update ([9adfad1](https://gitlab.com/saastack/ui/customers/commit/9adfad153f3082818d5868ff88abb2cde8a30107))
-   relay update ([f2430af](https://gitlab.com/saastack/ui/customers/commit/f2430af1dbe68f34c8c97218e0e47977e66f1d88))

### [0.1.121](https://gitlab.com/saastack/ui/customers/compare/v0.1.120...v0.1.121) (2020-08-18)

### Bug Fixes

-   relay update ([f2430af](https://gitlab.com/saastack/ui/customers/commit/f2430af1dbe68f34c8c97218e0e47977e66f1d88))

### [0.1.120](https://gitlab.com/saastack/ui/customers/compare/v0.1.119...v0.1.120) (2020-08-13)

### Bug Fixes

-   customer update delete in
    appointemnt ([308aafa](https://gitlab.com/saastack/ui/customers/commit/308aafa0f202d155c9219597addf20eb71005f5e))

### [0.1.119](https://gitlab.com/saastack/ui/customers/compare/v0.1.118...v0.1.119) (2020-08-13)

### Bug Fixes

-   customer update delete in
    appointemnt ([0e13b35](https://gitlab.com/saastack/ui/customers/commit/0e13b35dc276eead2cd408403d9b8eb68fed2164))

### [0.1.118](https://gitlab.com/saastack/ui/customers/compare/v0.1.117...v0.1.118) (2020-08-12)

### Bug Fixes

-   alias in empty
    state ([a9a3302](https://gitlab.com/saastack/ui/customers/commit/a9a330228696a43b526836c28fb4130d3774af8a))

### [0.1.117](https://gitlab.com/saastack/ui/customers/compare/v0.1.116...v0.1.117) (2020-07-28)

### Bug Fixes

-   **release:** event subscribe
    fixes ([8be0c7c](https://gitlab.com/saastack/ui/customers/commit/8be0c7ce67cfc8398a83bbad8caef5e5142044a8))

### [0.1.116](https://gitlab.com/saastack/ui/customers/compare/v0.1.115...v0.1.116) (2020-07-22)

### Bug Fixes

-   refetch after
    bulkimport ([fcd0241](https://gitlab.com/saastack/ui/customers/commit/fcd024142ab32752376b23211f73cb9c5bf42d03))

### [0.1.115](https://gitlab.com/saastack/ui/customers/compare/v0.1.114...v0.1.115) (2020-07-21)

### Bug Fixes

-   refetching after
    bulkimport ([0154743](https://gitlab.com/saastack/ui/customers/commit/015474329bc2fd58a0a21346a912c73ec03cba09))

### [0.1.114](https://gitlab.com/saastack/ui/customers/compare/v0.1.113...v0.1.114) (2020-07-21)

### Bug Fixes

-   alias ([c01c61b](https://gitlab.com/saastack/ui/customers/commit/c01c61b568583940a525aaf95c0705bcb627de28))

### [0.1.113](https://gitlab.com/saastack/ui/customers/compare/v0.1.112...v0.1.113) (2020-07-21)

### Bug Fixes

-   alias ([2d47a83](https://gitlab.com/saastack/ui/customers/commit/2d47a830478e2fb0b0e35cf5524358cad2cb43ba))

### [0.1.112](https://gitlab.com/saastack/ui/customers/compare/v0.1.111...v0.1.112) (2020-07-20)

### Features

-   alias ([2bb4ba9](https://gitlab.com/saastack/ui/customers/commit/2bb4ba92e5116a6c2660372dad4f09603681dddf))

### Bug Fixes

-   alias ([c726215](https://gitlab.com/saastack/ui/customers/commit/c7262157436c4eee3fdf3aef04c0c6f0d37eaae2))

### [0.1.111](https://gitlab.com/saastack/ui/customers/compare/v0.1.110...v0.1.111) (2020-07-17)

### Bug Fixes

-   refetch notes on
    delete ([b6e4268](https://gitlab.com/saastack/ui/customers/commit/b6e4268f029d9d24fa8427bf98f9fd53e1ed8f0c))

### [0.1.110](https://gitlab.com/saastack/ui/customers/compare/v0.1.109...v0.1.110) (2020-07-17)

### Bug Fixes

-   refetch notes on
    delete ([2b9a897](https://gitlab.com/saastack/ui/customers/commit/2b9a897076f24b0bf2a4395d017ab328d2390f84))

### [0.1.109](https://gitlab.com/saastack/ui/customers/compare/v0.1.108...v0.1.109) (2020-07-16)

### Features

-   **bulkimport:** added sample data and
    autofill ([0239128](https://gitlab.com/saastack/ui/customers/commit/0239128e697dd998e1ec004705e8bedbed87e50a))

### [0.1.108](https://gitlab.com/saastack/ui/customers/compare/v0.1.107...v0.1.108) (2020-07-14)

### Bug Fixes

-   location admin unable to add
    tags ([0e61a03](https://gitlab.com/saastack/ui/customers/commit/0e61a035050a52f804f444af70c19f517756f421))
-   open customer form from location admin
    view ([5b3c3d5](https://gitlab.com/saastack/ui/customers/commit/5b3c3d56bf18e182fcb8b4a38d0b7cc58e33e99d))

### [0.1.107](https://gitlab.com/saastack/ui/customers/compare/v0.1.106...v0.1.107) (2020-07-13)

### Bug Fixes

-   fill customer address in
    update ([9604237](https://gitlab.com/saastack/ui/customers/commit/960423787ff257d1f414f3f79c061dea89988e0a))

### [0.1.106](https://gitlab.com/saastack/ui/customers/compare/v0.1.105...v0.1.106) (2020-07-13)

### Bug Fixes

-   refetch customers on adding a new
    customer ([a8fc4df](https://gitlab.com/saastack/ui/customers/commit/a8fc4dfb7d142ea2e241d80b6f15b304fb80fe1b))

### [0.1.105](https://gitlab.com/saastack/ui/customers/compare/v0.1.104...v0.1.105) (2020-07-11)

### [0.1.104](https://gitlab.com/saastack/ui/customers/compare/v0.1.103...v0.1.104) (2020-07-04)

### [0.1.103](https://gitlab.com/saastack/ui/customers/compare/v0.1.102...v0.1.103) (2020-07-04)

### Bug Fixes

-   minor bug fixes ([52da91b](https://gitlab.com/saastack/ui/customers/commit/52da91bf02b8841160bd8be1e7173fbdf9a5133d))

### [0.1.102](https://gitlab.com/saastack/ui/customers/compare/v0.1.101...v0.1.102) (2020-07-02)

### [0.1.101](https://gitlab.com/saastack/ui/customers/compare/v0.1.100...v0.1.101) (2020-07-02)

### Bug Fixes

-   bulkimport
    transformations ([0c706d3](https://gitlab.com/saastack/ui/customers/commit/0c706d3545505326be1e32d9240d046b32fe5144))
-   validation of
    bulkimport ([8b1dceb](https://gitlab.com/saastack/ui/customers/commit/8b1dcebce068e360ff11a41b2e803d585d811340))

### [0.1.100](https://gitlab.com/saastack/ui/customers/compare/v0.1.99...v0.1.100) (2020-07-01)

### Bug Fixes

-   package versions ([68bf656](https://gitlab.com/saastack/ui/customers/commit/68bf656e89b7b3132f39659bd7fbfd77b4924bcb))

### [0.1.99](https://gitlab.com/saastack/ui/customers/compare/v0.1.98...v0.1.99) (2020-06-29)

### [0.1.98](https://gitlab.com/saastack/ui/customers/compare/v0.1.97...v0.1.98) (2020-06-27)

### Bug Fixes

-   fixed
    validations ([d210123](https://gitlab.com/saastack/ui/customers/commit/d2101235008de3b1ba2332a8875b3f565791e17f))

### [0.1.97](https://gitlab.com/saastack/ui/customers/compare/v0.1.96...v0.1.97) (2020-06-27)

### Bug Fixes

-   fixed
    validations ([72843f2](https://gitlab.com/saastack/ui/customers/commit/72843f2dd9badcf3b7c99aba5fa18895312fe18a))

### [0.1.96](https://gitlab.com/saastack/ui/customers/compare/v0.1.95...v0.1.96) (2020-06-26)

### Bug Fixes

-   transformation in bulk import
    component ([8ae7a9b](https://gitlab.com/saastack/ui/customers/commit/8ae7a9b7b4b7c1809921ba468d2786800b851796))
-   transormation in bulkimport
    component ([f94a944](https://gitlab.com/saastack/ui/customers/commit/f94a9445906f00b1178efd58391f609f7ba37463))

### [0.1.95](https://gitlab.com/saastack/ui/customers/compare/v0.1.93...v0.1.95) (2020-06-26)

### Bug Fixes

-   bulkimport steps ([b624146](https://gitlab.com/saastack/ui/customers/commit/b624146ed724963c2f3b6c4f68752be31c3221bc))
-   store issue ([fb7c9c9](https://gitlab.com/saastack/ui/customers/commit/fb7c9c9d24f14b5b715f316b11af030659d7be54))
-   **release:** version
    update ([4a61bce](https://gitlab.com/saastack/ui/customers/commit/4a61bce6036f7d73679fb39628754f12a9de4f26))

### [0.1.92](https://gitlab.com/saastack/ui/customers/compare/v0.1.91...v0.1.92) (2020-06-26)

### Features

-   added validations and transformationin bulk
    import ([d58b18d](https://gitlab.com/saastack/ui/customers/commit/d58b18ddde36f321bf1c6def43f2198939197e3a))

### Bug Fixes

-   store issue ([fb7c9c9](https://gitlab.com/saastack/ui/customers/commit/fb7c9c9d24f14b5b715f316b11af030659d7be54))

### [0.1.91](https://gitlab.com/saastack/ui/customers/compare/v0.1.90...v0.1.91) (2020-06-20)

### Bug Fixes

-   fixed pagination ([202d83b](https://gitlab.com/saastack/ui/customers/commit/202d83bc2dac3660d31bfe42114d448f961330bd))

### [0.1.90](https://gitlab.com/saastack/ui/customers/compare/v0.1.89...v0.1.90) (2020-06-20)

### Bug Fixes

-   fixed sales
    report ([86d44e1](https://gitlab.com/saastack/ui/customers/commit/86d44e176339b68c0075dbc18ca5847e19667909))

### [0.1.89](https://gitlab.com/saastack/ui/customers/compare/v0.1.88...v0.1.89) (2020-06-20)

### Bug Fixes

-   dark mode in
    charts ([05981e2](https://gitlab.com/saastack/ui/customers/commit/05981e2329d0304952ff1d31a5e36098ab17dcaa))

### [0.1.88](https://gitlab.com/saastack/ui/customers/compare/v0.1.87...v0.1.88) (2020-06-18)

### Bug Fixes

-   location ids fix in location
    admin ([18bb186](https://gitlab.com/saastack/ui/customers/commit/18bb18658f1a3190f26246c4a80cb9c823bc683a))

### [0.1.87](https://gitlab.com/saastack/ui/customers/compare/v0.1.86...v0.1.87) (2020-06-17)

### Bug Fixes

-   detail ([72c1645](https://gitlab.com/saastack/ui/customers/commit/72c1645a6e3da289c3e18ae1c8acdd3dc9cbd687))

### [0.1.86](https://gitlab.com/saastack/ui/customers/compare/v0.1.85...v0.1.86) (2020-06-17)

### Bug Fixes

-   fixed custoemr detail
    page ([6186f1e](https://gitlab.com/saastack/ui/customers/commit/6186f1e55240a7c48eefec8b9f17533b26a280c6))

### [0.1.85](https://gitlab.com/saastack/ui/customers/compare/v0.1.84...v0.1.85) (2020-06-17)

### Bug Fixes

-   fixed custoemr detail
    page ([57243c1](https://gitlab.com/saastack/ui/customers/commit/57243c12330dda3406a69d59d8118a9be1bcc6e8))

### [0.1.84](https://gitlab.com/saastack/ui/customers/compare/v0.1.83...v0.1.84) (2020-06-17)

### Bug Fixes

-   fixed notes ([1d59ac9](https://gitlab.com/saastack/ui/customers/commit/1d59ac9cfcff5600a15f0c0a8fd794c258190320))

### [0.1.83](https://gitlab.com/saastack/ui/customers/compare/v0.1.82...v0.1.83) (2020-06-16)

### Bug Fixes

-   fixed rights ([f87be9d](https://gitlab.com/saastack/ui/customers/commit/f87be9d41bb2fdb490e371dbd89450944b92b4d6))

### [0.1.82](https://gitlab.com/saastack/ui/customers/compare/v0.1.81...v0.1.82) (2020-06-16)

### Bug Fixes

-   fixed rights ([0bb3672](https://gitlab.com/saastack/ui/customers/commit/0bb3672be157683fc0867b592e0c3adf6e70ea09))

### [0.1.81](https://gitlab.com/saastack/ui/customers/compare/v0.1.80...v0.1.81) (2020-06-15)

### Bug Fixes

-   added locations ([2cce478](https://gitlab.com/saastack/ui/customers/commit/2cce478a46a5fbf600f7d0717dab24775122f550))

### [0.1.80](https://gitlab.com/saastack/ui/customers/compare/v0.1.79...v0.1.80) (2020-06-15)

### Bug Fixes

-   added locations ([c97f829](https://gitlab.com/saastack/ui/customers/commit/c97f82998b1018ce96cf423848117f6749f08a43))

### [0.1.79](https://gitlab.com/saastack/ui/customers/compare/v0.1.78...v0.1.79) (2020-06-15)

### Bug Fixes

-   fixed header
    border ([5001fc5](https://gitlab.com/saastack/ui/customers/commit/5001fc524bb65e482e492524e347809b65fd3f4e))

### [0.1.78](https://gitlab.com/saastack/ui/customers/compare/v0.1.77...v0.1.78) (2020-06-15)

### Bug Fixes

-   fixed customer
    rights ([39a0adf](https://gitlab.com/saastack/ui/customers/commit/39a0adf016e864aade3376a1b46a0032f8a7813a))

### [0.1.77](https://gitlab.com/saastack/ui/customers/compare/v0.1.76...v0.1.77) (2020-06-15)

### Bug Fixes

-   fixed customer
    rights ([5a1fb3d](https://gitlab.com/saastack/ui/customers/commit/5a1fb3dfe6fa8b9714c3857024255de3da4e1016))

### [0.1.76](https://gitlab.com/saastack/ui/customers/compare/v0.1.75...v0.1.76) (2020-06-13)

### Bug Fixes

-   fixed location admin
    rights ([65d185d](https://gitlab.com/saastack/ui/customers/commit/65d185d13ab4bde94a33fee1e8fc8b7c59a21220))

### [0.1.75](https://gitlab.com/saastack/ui/customers/compare/v0.1.74...v0.1.75) (2020-06-13)

### Bug Fixes

-   access contact
    location ([ccecfed](https://gitlab.com/saastack/ui/customers/commit/ccecfedcb29206683168510ae9a7d511cf150bcb))

### [0.1.74](https://gitlab.com/saastack/ui/customers/compare/v0.1.73...v0.1.74) (2020-06-13)

### Bug Fixes

-   fixed customer ([1eac73d](https://gitlab.com/saastack/ui/customers/commit/1eac73d1af7768be1f96d0b74bee4559f3998acb))

### [0.1.73](https://gitlab.com/saastack/ui/customers/compare/v0.1.72...v0.1.73) (2020-06-13)

### Bug Fixes

-   fixed css ([d4678ca](https://gitlab.com/saastack/ui/customers/commit/d4678caa2f5948bd07159c2bd675623e96915263))

### [0.1.72](https://gitlab.com/saastack/ui/customers/compare/v0.1.71...v0.1.72) (2020-06-13)

### Bug Fixes

-   customer app names
    filtered ([b63b36f](https://gitlab.com/saastack/ui/customers/commit/b63b36fc734f7986ca7c1092288333190fcd9fa1))
-   customer rights
    fixed ([14a1476](https://gitlab.com/saastack/ui/customers/commit/14a14769be9c2bbd1415cfeddf5bd8dc8003e345))

### [0.1.71](https://gitlab.com/saastack/ui/customers/compare/v0.1.70...v0.1.71) (2020-06-12)

### Bug Fixes

-   customer notes
    hook ([6d641d2](https://gitlab.com/saastack/ui/customers/commit/6d641d23e38837392af4964a6a3d9a82af08a15c))

### [0.1.70](https://gitlab.com/saastack/ui/customers/compare/v0.1.69...v0.1.70) (2020-06-12)

### Bug Fixes

-   customer filters ([bdf53cf](https://gitlab.com/saastack/ui/customers/commit/bdf53cf906695b114c11c097e398ceeabffe2d2b))

### [0.1.69](https://gitlab.com/saastack/ui/customers/compare/v0.1.68...v0.1.69) (2020-06-12)

### Bug Fixes

-   customer ux ([b1622a6](https://gitlab.com/saastack/ui/customers/commit/b1622a6b2905a5816bdb80cf7404a500341004cc))

### [0.1.68](https://gitlab.com/saastack/ui/customers/compare/v0.1.67...v0.1.68) (2020-06-11)

### Bug Fixes

-   customer ux ([fa544bb](https://gitlab.com/saastack/ui/customers/commit/fa544bba5a3a244bc122df0e266416c754dc2d18))

### [0.1.67](https://gitlab.com/saastack/ui/customers/compare/v0.1.66...v0.1.67) (2020-06-11)

### Bug Fixes

-   customer ux ([2750885](https://gitlab.com/saastack/ui/customers/commit/2750885571a4da68f9c581d74975f6914dab72ec))

### [0.1.66](https://gitlab.com/saastack/ui/customers/compare/v0.1.65...v0.1.66) (2020-06-11)

### Bug Fixes

-   customer ux ([b397aae](https://gitlab.com/saastack/ui/customers/commit/b397aae4ef0b75fba50fd64b2f3a3eb1e06e9c04))

### [0.1.65](https://gitlab.com/saastack/ui/customers/compare/v0.1.64...v0.1.65) (2020-06-11)

### Bug Fixes

-   customer ux ([2467f3f](https://gitlab.com/saastack/ui/customers/commit/2467f3ff631cfe0d0e93cab58e42c201b3501ce6))

### [0.1.64](https://gitlab.com/saastack/ui/customers/compare/v0.1.63...v0.1.64) (2020-06-11)

### Bug Fixes

-   customer ux ([c7cb34a](https://gitlab.com/saastack/ui/customers/commit/c7cb34a050abf969c275db6d907dbb7649eb19d1))

### [0.1.63](https://gitlab.com/saastack/ui/customers/compare/v0.1.62...v0.1.63) (2020-06-10)

### Bug Fixes

-   customer ux ([6fa74d4](https://gitlab.com/saastack/ui/customers/commit/6fa74d461c191165e69cc631d94cea10e09db514))

### [0.1.62](https://gitlab.com/saastack/ui/customers/compare/v0.1.61...v0.1.62) (2020-06-01)

### Features

-   customer view
    action ([6dee162](https://gitlab.com/saastack/ui/customers/commit/6dee16279a53b3f58ef162bb9fc635c497927f9e))

### Bug Fixes

-   web notification
    action ([e31792b](https://gitlab.com/saastack/ui/customers/commit/e31792bac7addca545cd75cf14b8b9a53fae87fb))

### [0.1.61](https://gitlab.com/saastack/ui/customers/compare/v0.1.60...v0.1.61) (2020-05-23)

### Features

-   graphs in grouped
    reports ([6761a1c](https://gitlab.com/saastack/ui/customers/commit/6761a1c5a20a38c35aca6024bd6ee970dd6755c6))

### Bug Fixes

-   remove filters, icon in
    parent ([2e6cebb](https://gitlab.com/saastack/ui/customers/commit/2e6cebb44cf868afde9fe9b68d2a578c59ba237f))

### [0.1.60](https://gitlab.com/saastack/ui/customers/compare/v0.1.59...v0.1.60) (2020-05-22)

### Features

-   groupings ([f8a02c7](https://gitlab.com/saastack/ui/customers/commit/f8a02c76c062a695f5d6d2ef98d197f6b6d46208))

### [0.1.59](https://gitlab.com/saastack/ui/customers/compare/v0.1.58...v0.1.59) (2020-05-21)

### Bug Fixes

-   export commented ([2c7e2bd](https://gitlab.com/saastack/ui/customers/commit/2c7e2bd5bb865c9f5cd8467fc452eef404e70d14))

### [0.1.58](https://gitlab.com/saastack/ui/customers/compare/v0.1.57...v0.1.58) (2020-05-21)

### Features

-   grouped reports ([23db2ed](https://gitlab.com/saastack/ui/customers/commit/23db2ed7d170eccc83e289d59f5514b0771d25b2))

### [0.1.57](https://gitlab.com/saastack/ui/customers/compare/v0.1.56...v0.1.57) (2020-05-21)

### Features

-   report filters on
    address ([584eef0](https://gitlab.com/saastack/ui/customers/commit/584eef027ee355616db6ed1e9fc9015c545fc751))

### [0.1.56](https://gitlab.com/saastack/ui/customers/compare/v0.1.55...v0.1.56) (2020-05-21)

### Bug Fixes

-   reports priority ([574cde0](https://gitlab.com/saastack/ui/customers/commit/574cde0131344a679abf21c465dc3d15758e8f64))

### [0.1.55](https://gitlab.com/saastack/ui/customers/compare/v0.1.54...v0.1.55) (2020-05-21)

### Bug Fixes

-   parent priority ([72c5737](https://gitlab.com/saastack/ui/customers/commit/72c5737724050dd4cbee40b1dbad7b6cb8d5954d))

### [0.1.54](https://gitlab.com/saastack/ui/customers/compare/v0.1.53...v0.1.54) (2020-05-20)

### Bug Fixes

-   parent priority ([1434027](https://gitlab.com/saastack/ui/customers/commit/14340276d77b8ddf59cd8f07f6e4c8de9c47c65a))

### [0.1.53](https://gitlab.com/saastack/ui/customers/compare/v0.1.52...v0.1.53) (2020-05-20)

### Features

-   reports ([c563605](https://gitlab.com/saastack/ui/customers/commit/c563605f5c1dc68a81616ad9150e259b89d44dca))

### [0.1.52](https://gitlab.com/saastack/ui/customers/compare/v0.1.51...v0.1.52) (2020-05-19)

### Bug Fixes

-   timezone in
    reports ([f0af092](https://gitlab.com/saastack/ui/customers/commit/f0af09280aad951874d9b8648cf6919148bc7b4d))

### [0.1.51](https://gitlab.com/saastack/ui/customers/compare/v0.1.50...v0.1.51) (2020-05-19)

### Bug Fixes

-   reports data ([7413b52](https://gitlab.com/saastack/ui/customers/commit/7413b520df2e89486cbe602e13d5372c7d1c6146))

### [0.1.50](https://gitlab.com/saastack/ui/customers/compare/v0.1.49...v0.1.50) (2020-05-19)

### Features

-   reports ([4667277](https://gitlab.com/saastack/ui/customers/commit/4667277c86501d16af899df660190f549e806b61))

### [0.1.49](https://gitlab.com/saastack/ui/customers/compare/v0.1.48...v0.1.49) (2020-05-15)

### Bug Fixes

-   level in add action
    hook ([f1c1f12](https://gitlab.com/saastack/ui/customers/commit/f1c1f12e2956c8b6194aac0d8e7536eb8040f8b0))

### [0.1.48](https://gitlab.com/saastack/ui/customers/compare/v0.1.47...v0.1.48) (2020-05-15)

### Bug Fixes

-   location parent add
    customer ([3600d50](https://gitlab.com/saastack/ui/customers/commit/3600d505cc58202408cb15a5f89b4ecd3b1275e3))

### [0.1.47](https://gitlab.com/saastack/ui/customers/compare/v0.1.46...v0.1.47) (2020-05-15)

### Bug Fixes

-   location parent add
    customer ([b925900](https://gitlab.com/saastack/ui/customers/commit/b9259006f6061c0ae0dd6b3ebc7f0216fad98027))

### [0.1.46](https://gitlab.com/saastack/ui/customers/compare/v0.1.45...v0.1.46) (2020-05-14)

### Features

-   alphabet list
    added ([e6afd59](https://gitlab.com/saastack/ui/customers/commit/e6afd59db9769c5ae746a8d48089f5556a226aba))

### Bug Fixes

-   customer add
    action ([ed5e8df](https://gitlab.com/saastack/ui/customers/commit/ed5e8df43e59ab1046a92907ea8cb6f3ec63dfcb))
-   customer alphabet list with
    search ([94ef275](https://gitlab.com/saastack/ui/customers/commit/94ef275f6c57b05bc04ac0b99ed976cbcea81ba0))

### [0.1.45](https://gitlab.com/saastack/ui/customers/compare/v0.1.44...v0.1.45) (2020-05-14)

### Bug Fixes

-   add action ([a0b384b](https://gitlab.com/saastack/ui/customers/commit/a0b384bbc940376e179a464666f0fa39bd87c288))
-   add action hook
    register ([dc110d7](https://gitlab.com/saastack/ui/customers/commit/dc110d75c3f3f653ffad1dd510972b02cd4d525b))
-   story ([878bd0f](https://gitlab.com/saastack/ui/customers/commit/878bd0fda063a1234ac6d5709ce7bb24b74cae4a))

### [0.1.44](https://gitlab.com/saastack/ui/customers/compare/v0.1.43...v0.1.44) (2020-05-12)

### Bug Fixes

-   priority ([6d5b367](https://gitlab.com/saastack/ui/customers/commit/6d5b3679b789c7ace7c2621f884dd1370dce1b2f))
-   stories ([e1c7980](https://gitlab.com/saastack/ui/customers/commit/e1c79807d3e3fad49f6e4791219d93d92fff34b2))

### [0.1.43](https://gitlab.com/saastack/ui/customers/compare/v0.1.42...v0.1.43) (2020-04-18)

### Bug Fixes

-   fix ux ([6f118b7](https://gitlab.com/saastack/ui/customers/commit/6f118b77160c693fc685e5725e5bedf6459b929b))

### [0.1.42](https://gitlab.com/saastack/ui/customers/compare/v0.1.41...v0.1.42) (2020-04-17)

### [0.1.41](https://gitlab.com/saastack/ui/customers/compare/v0.1.40...v0.1.41) (2020-04-17)

### Features

-   aliasing ([27208e2](https://gitlab.com/saastack/ui/customers/commit/27208e2d042b8f81bdfef99b4eeff752c50eda38))

### [0.1.40](https://gitlab.com/saastack/ui/customers/compare/v0.1.39...v0.1.40) (2020-04-16)

### Features

-   alias hook ([febf689](https://gitlab.com/saastack/ui/customers/commit/febf68908a2e711b9ba058972ea7932fe1e9b1a3))

### [0.1.39](https://gitlab.com/saastack/ui/customers/compare/v0.1.38...v0.1.39) (2020-04-15)

### Bug Fixes

-   fix anme spliy ([2ed2241](https://gitlab.com/saastack/ui/customers/commit/2ed224195f6d0d979585ec8f06fd487916ffaa4b))

### [0.1.38](https://gitlab.com/saastack/ui/customers/compare/v0.1.37...v0.1.38) (2020-04-15)

### Bug Fixes

-   fix anme spliy ([2ed2241](https://gitlab.com/saastack/ui/customers/commit/2ed224195f6d0d979585ec8f06fd487916ffaa4b))

### [0.1.37](https://gitlab.com/saastack/ui/customers/compare/v0.1.36...v0.1.37) (2020-04-14)

### Bug Fixes

-   fix ([dc21d20](https://gitlab.com/saastack/ui/customers/commit/dc21d203030913b5134fa59209071aa2d5313803))

### [0.1.36](https://gitlab.com/saastack/ui/customers/compare/v0.1.35...v0.1.36) (2020-04-08)

### Features

-   show switcher based on
    role ([99347f4](https://gitlab.com/saastack/ui/customers/commit/99347f429bc4b61ae25b3f74512cc29b3aadebce))

### [0.1.35](https://gitlab.com/saastack/ui/customers/compare/v0.1.34...v0.1.35) (2020-04-07)

### Bug Fixes

-   send parent on the basis of
    roles ([cc447f3](https://gitlab.com/saastack/ui/customers/commit/cc447f3f5054a59c15e26651cc073aaa163842ea))

### [0.1.34](https://gitlab.com/saastack/ui/customers/compare/v0.1.33...v0.1.34) (2020-04-07)

### Bug Fixes

-   roles in settings
    hook ([80d2f8d](https://gitlab.com/saastack/ui/customers/commit/80d2f8d0a8cb6d8030ca8d6288832a900b467e88))

### [0.1.33](https://gitlab.com/saastack/ui/customers/compare/v0.1.32...v0.1.33) (2020-03-28)

### Bug Fixes

-   fix avatar click ([524a29d](https://gitlab.com/saastack/ui/customers/commit/524a29d705ee0eecca8579b4277c90c3b5ccf572))

### [0.1.32](https://gitlab.com/saastack/ui/customers/compare/v0.1.31...v0.1.32) (2020-03-28)

### Bug Fixes

-   search fix ([c8a1892](https://gitlab.com/saastack/ui/customers/commit/c8a1892ee6ccc7389f9042bfbb822767ce95c60c))

### [0.1.31](https://gitlab.com/saastack/ui/customers/compare/v0.1.30...v0.1.31) (2020-03-28)

### Bug Fixes

-   delete navigate back
    fix ([2992c74](https://gitlab.com/saastack/ui/customers/commit/2992c7428f3a2384a51c3492b8f2e02b8258fe34))

### [0.1.30](https://gitlab.com/saastack/ui/customers/compare/v0.1.29...v0.1.30) (2020-03-27)

### Bug Fixes

-   search fixed ([d56253a](https://gitlab.com/saastack/ui/customers/commit/d56253a4ec5ba3478f3e5b4f77f82ed0fbca0205))

### [0.1.29](https://gitlab.com/saastack/ui/customers/compare/v0.1.28...v0.1.29) (2020-03-27)

### Bug Fixes

-   search fixed ([b6e6ce5](https://gitlab.com/saastack/ui/customers/commit/b6e6ce5ece97ae13215673cbc5f6549065a2e830))
-   search fixed ([fc3a145](https://gitlab.com/saastack/ui/customers/commit/fc3a145577a7c632f6efb141263740fa21c41130))

### [0.1.28](https://gitlab.com/saastack/ui/customers/compare/v0.1.27...v0.1.28) (2020-03-26)

### Bug Fixes

-   bug fix ([9c3e78e](https://gitlab.com/saastack/ui/customers/commit/9c3e78e0ac725d8a94843a1adabda631b162a4a7))

### [0.1.27](https://gitlab.com/saastack/ui/customers/compare/v0.1.26...v0.1.27) (2020-03-25)

### [0.1.26](https://gitlab.com/saastack/ui/customers/compare/v0.1.25...v0.1.26) (2020-03-25)

### [0.1.25](https://gitlab.com/saastack/ui/customers/compare/v0.1.24...v0.1.25) (2020-03-25)

### Bug Fixes

-   parent change ([379e4af](https://gitlab.com/saastack/ui/customers/commit/379e4af516c2b8f4cd9384a25190dbdd31e7ac37))

### [0.1.24](https://gitlab.com/saastack/ui/customers/compare/v0.1.23...v0.1.24) (2020-03-25)

### Bug Fixes

-   customer search ([b0d1762](https://gitlab.com/saastack/ui/customers/commit/b0d176252b484ac0a94f4d549157b1e75857a34d))
-   remove customer
    delete ([f83f126](https://gitlab.com/saastack/ui/customers/commit/f83f12680e06a5ce08b2556ca6d132f7faf0e25b))
-   update customer ([713325b](https://gitlab.com/saastack/ui/customers/commit/713325ba4179a9698745ae2fa088eb27a6f082c6))

### [0.1.23](https://gitlab.com/saastack/ui/customers/compare/v0.1.22...v0.1.23) (2020-03-25)

### Bug Fixes

-   add customer form, phone number country and
    message ([5c794b3](https://gitlab.com/saastack/ui/customers/commit/5c794b35314621002e7b2bfdc6a2999436cfdaf1))
-   refresh to see newly created
    customer ([3b9b6ae](https://gitlab.com/saastack/ui/customers/commit/3b9b6aeff13de808f46a98468f48c9dacae8265c))

### [0.1.22](https://gitlab.com/saastack/ui/customers/compare/v0.1.21...v0.1.22) (2020-03-24)

### Bug Fixes

-   condition ([86d95c0](https://gitlab.com/saastack/ui/customers/commit/86d95c019d895c0de48e3ffaa8d2d83fcf07fc47))

### [0.1.21](https://gitlab.com/saastack/ui/customers/compare/v0.1.20...v0.1.21) (2020-03-24)

### Bug Fixes

-   condition ([71d7af3](https://gitlab.com/saastack/ui/customers/commit/71d7af34dc8f6edbfcf8e032874511034f3395ef))
-   layout ([47c1aa3](https://gitlab.com/saastack/ui/customers/commit/47c1aa34d8b45a41d16e939f80b0210cb49e48eb))

### [0.1.20](https://gitlab.com/saastack/ui/customers/compare/v0.1.19...v0.1.20) (2020-03-24)

### Bug Fixes

-   layout ([78e8e65](https://gitlab.com/saastack/ui/customers/commit/78e8e658ad8b63ccba0aa08ef31065233f149cc4))
-   **correction:**
    correction ([953aeb5](https://gitlab.com/saastack/ui/customers/commit/953aeb5151b5145a7476c306df6a22d69c334244))
-   **fix:** layout
    change ([bc8de19](https://gitlab.com/saastack/ui/customers/commit/bc8de192b29caae4f3ff3cd0fc66c03c2788a40f))

### [0.1.19](https://gitlab.com/saastack/ui/customers/compare/v0.1.17...v0.1.19) (2020-03-11)

### Bug Fixes

-   **release:** image upload
    fixes ([b9e9a4f](https://gitlab.com/saastack/ui/customers/commit/b9e9a4fc74fbedffe0f8a39bab968eccce5f4603))
-   **sub app icon:** changed the icon to people
    outlined ([6ea4ebd](https://gitlab.com/saastack/ui/customers/commit/6ea4ebde36cbd2b1ad1b2f35b46bafbd5906f9dd))

### [0.1.17](https://gitlab.com/saastack/ui/customers/compare/v0.1.16...v0.1.17) (2020-02-28)

### [0.1.16](https://gitlab.com/saastack/ui/customers/compare/v0.1.15...v0.1.16) (2020-02-28)

### Bug Fixes

-   **release:** customer id pass inside tab
    hook ([3ca589c](https://gitlab.com/saastack/ui/customers/commit/3ca589cad8beeb7a5357f6f43b8144f43f993f32))
-   **release:** generated
    added ([d965cd7](https://gitlab.com/saastack/ui/customers/commit/d965cd7ee14039d522984a58c9707a2eb354ace0))

### [0.1.15](https://gitlab.com/saastack/ui/customers/compare/v0.1.14...v0.1.15) (2020-02-18)

### Bug Fixes

-   **release:** Path & icon
    fix ([6dea7fb](https://gitlab.com/saastack/ui/customers/commit/6dea7fb8b7c330f8312b84605cdcf613b028efaa))

### [0.1.14](https://gitlab.com/saastack/ui/customers/compare/v0.1.13...v0.1.14) (2020-02-17)

### Bug Fixes

-   **release:** image avatar temp
    fix ([e5d6301](https://gitlab.com/saastack/ui/customers/commit/e5d63013e3943bb0b117ce250028d07d1e945575))

### [0.1.13](https://gitlab.com/saastack/ui/customers/compare/v0.1.12...v0.1.13) (2020-02-17)

### [0.1.12](https://gitlab.com/saastack/ui/customers/compare/v0.1.11...v0.1.12) (2020-02-15)

### [0.1.11](https://gitlab.com/saastack/ui/customers/compare/v0.1.10...v0.1.11) (2020-02-15)

### [0.1.10](https://gitlab.com/saastack/ui/customers/compare/v0.1.9...v0.1.10) (2020-02-15)

### [0.1.9](https://gitlab.com/saastack/ui/customers/compare/v0.1.8...v0.1.9) (2020-02-15)

### [0.1.8](https://gitlab.com/saastack/ui/customers/compare/v0.1.7...v0.1.8) (2020-02-15)

### [0.1.7](https://gitlab.com/saastack/ui/customers/compare/v0.1.6...v0.1.7) (2020-02-15)

### [0.1.6](https://gitlab.com/saastack/ui/customers/compare/v0.1.5...v0.1.6) (2020-02-15)

### [0.1.5](https://gitlab.com/saastack/ui/customers/compare/v0.1.4...v0.1.5) (2020-02-15)

### [0.1.4](https://gitlab.com/saastack/ui/customers/compare/v0.1.3...v0.1.4) (2020-02-15)

### [0.1.3](https://gitlab.com/saastack/ui/customers/compare/v0.1.2...v0.1.3) (2020-02-15)

### [0.1.2](https://gitlab.com/saastack/ui/customers/compare/v0.1.1...v0.1.2) (2020-02-15)

### 0.1.1 (2020-02-15)

### Bug Fixes

-   saastack
    migration ([9f496fc](https://gitlab.com/saastack/ui/customers/commit/9f496fc7040237d0864cb02f0dec1f3cd5aef58d))
